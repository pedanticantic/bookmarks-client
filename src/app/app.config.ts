import {ApplicationConfig} from '@angular/core';
import {provideHttpClient} from '@angular/common/http';
import {ApiConfigService} from './services/api-config.service';
import {ActivatedRoute, provideRouter} from '@angular/router';
import {appRoutes} from './app.routing';
import {RegisterService} from './services/register.service';
import {ApiCallerService} from './services/api-caller.service';
import {AuthenticationService} from './services/authentication.service';
import {AuthGuard} from './guards/auth-guard';
import {ModalService} from './modal/modal.service';
import {GroupService} from './services/group.service';
import {FilterService} from './services/filter.service';
import {PageService} from './services/page.service';
import {provideAnimations} from '@angular/platform-browser/animations';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(appRoutes),
    provideHttpClient(),
    provideAnimations(),
    ApiConfigService
    /*, ActivatedRoute*/,
    RegisterService,
    ApiCallerService,
    AuthenticationService,
    AuthGuard,
    ModalService,
    GroupService,
    FilterService,
    PageService
  ]
};
