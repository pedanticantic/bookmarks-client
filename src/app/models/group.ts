export class Group {
    id: number;
    name: string;
    createdByUserId: number;

    public static makeGroup(groupData): Group
    {
        let group = new Group();

        group.id = groupData.id;
        group.name = groupData.name;
        group.createdByUserId = groupData.createdByUserId;

        return group;
    }
}