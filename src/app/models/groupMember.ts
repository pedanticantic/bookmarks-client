export class GroupMember {
    public static readonly STATUS_INVITED: string = 'Invited';
    public static readonly STATUS_ACCEPTED: string = 'Accepted';
    public static readonly STATUS_DECLINED: string = 'Declined';

    public static readonly ROLE_ADMIN: string = 'Admin';
    public static readonly ROLE_MEMBER: string = 'Member';

    // @TODO: Change this so we have a reference to an instance of Group and an instance of ?User.
    // @TODO: And of course, update all the references to it.
    groupId: number;
    groupName: string;
    createdByUserId: number;
    role: string;
    status: string;
    userId: number;
    userGivenName: string;
    userFamilyName: string;

    public static makeGroupMember(groupMemberData): GroupMember {
        // groupMemberData will be an object with 3 properties: 'group', 'groupMember' and 'user'.
        let group = new GroupMember();

        group.groupId = groupMemberData.group.id;
        group.groupName = groupMemberData.group.name;
        group.createdByUserId = groupMemberData.group.createdByUserId;

        group.role = groupMemberData.groupMember.role;
        group.status = groupMemberData.groupMember.status;

        group.userId = groupMemberData.user.id;
        group.userGivenName = groupMemberData.user.hasOwnProperty('givenName') ? groupMemberData.user.givenName : groupMemberData.user.name;
        group.userFamilyName = groupMemberData.user.familyName;

        return group;
    }

    // Method to accept an array of group members, and sort them by group name.
    public static sortByGroupName(groups: GroupMember[]) {
        groups.sort(function (group1: GroupMember, group2: GroupMember) {
            return (group1.groupName == group2.groupName ? 0 : (group1.groupName > group2.groupName ? 1 : -1));
        });
    }

    // Method to accept an array of group members, and sort them by member name.
    public static sortByMemberName(members: GroupMember[]) {
        members.sort(function (member1: GroupMember, member2: GroupMember) {
            if (member1.userFamilyName == member2.userFamilyName) {
                return (member1.userGivenName == member2.userGivenName ? 0 : (member1.userGivenName > member2.userGivenName ? 1 : -1));
            }
            return (member1.userFamilyName == member2.userFamilyName ? 0 : (member1.userFamilyName > member2.userFamilyName ? 1 : -1));
        });
    }

    public getStatus(): string {
        return this.status == GroupMember.STATUS_INVITED ? 'Pending' : GroupMember.STATUS_ACCEPTED;
    }

    public getRoles() {
        return [GroupMember.ROLE_ADMIN, GroupMember.ROLE_MEMBER];
    }

    public canResendInvitation(): boolean {
        return this.status == GroupMember.STATUS_INVITED;
    }
}