import { Node } from "./node";
import {AbstractElement} from "./abstractElement";

export class Grid extends AbstractElement {
    public static readonly NODE_TYPE = 'grid';

    id: number;
    nodeId: number;
    // Other properties TBC

    public static makeGrid(entity, parentNode: Node) {
        let grid = new Grid();

        grid.id = entity.id;
        grid.nodeId = entity.nodeId;
        // grid.heading = entity.heading; @TODO: Properties TBA

        grid.parentNode = parentNode;

        return grid;
    }

    filterMatches(filter: string): boolean {
        return false; // There is nothing to filter a grid on.
    }
}