// @TODO: Maybe move this model to a different folder?
import { Node } from "./node";
import {AbstractElement} from "./abstractElement";

export class Panel extends AbstractElement {
    public static readonly NODE_TYPE = 'panel';

    id: number;
    nodeId: number;
    title: string;

    public static makePanel(entity, parentNode: Node) {
        let panel = new Panel();

        panel.id = entity.id;
        panel.nodeId = entity.nodeId;
        panel.title = entity.title;

        panel.parentNode = parentNode;

        return panel;
    }

    filterMatches(filter: string): boolean {
        return this.title.toLowerCase().indexOf(filter) != -1;
    }
}