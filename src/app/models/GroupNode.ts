import {Group} from "./group";
import {Node} from "./node";

export class GroupNode {
    group: Group;
    node: Node;
    inPage: boolean;

    public static makeGroupNode(groupNodeData): GroupNode {
        let groupNode = new GroupNode();

        groupNode.group = Group.makeGroup(groupNodeData.group);
        groupNode.node = Node.makeNode(groupNodeData.node);

        return groupNode;
    }

    // Method to accept an array of group nodes, and sort them by owner, then type, then just id.
    public static sortByOwnerAndType(groupNodes: GroupNode[]) {
        groupNodes.sort(function (node1: GroupNode, node2: GroupNode) {
            if (node1.node.userId !+ node2.node.userId) {
                return node1.node.userId > node2.node.userId ? 1 : -1;
            }
            if (node1.node.nodeType !+ node2.node.nodeType) {
                return node1.node.nodeType > node2.node.nodeType ? 1 : -1;
            }

            return node1.node.id > node2.node.id ? 1 : -1;
        });
    }

    public setInPage(isInPage: boolean) {
        this.inPage = isInPage;
    }

    public isInPage(): boolean {
        return this.inPage;
    }
}