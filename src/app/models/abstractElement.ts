import { Node } from "./node";

export abstract class AbstractElement {
    parentNode: Node;

    abstract filterMatches(filter: string): boolean;
}