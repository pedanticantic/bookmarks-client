import { Node } from "./node";
import { Label } from "./label";
import {AbstractElement} from "./abstractElement";

export class Bookmark extends AbstractElement {
    public static readonly NODE_TYPE = 'bookmark';

    id: number;
    nodeId: number;
    label: string = '';
    url: string = '';

    labels: Label[] = [];

    public static makeBookmark(entity, parentNode: Node) {
        let bookmark = new Bookmark();

        bookmark.id = entity.id;
        bookmark.nodeId = entity.nodeId;
        bookmark.label = entity.label;
        bookmark.url = entity.url;
        bookmark.setLabelsFromRaw(entity.labels);

        bookmark.parentNode = parentNode;

        return bookmark;
    }

    filterMatches(filter: string): boolean {
        // Bookmarks have 3 things we can test the filter against:
        //  the label (link text)
        //  the URL
        //  the labels
        return this.label.toLowerCase().indexOf(filter) != -1 ||
            this.url.toLowerCase().indexOf(filter) != -1 ||
            this.hasMatchingLabels(filter);
    }

    private hasMatchingLabels(filter: string) {
        return this.labels.filter(
            function (label: Label): boolean {
                return label.getName().toLowerCase().indexOf(filter) != -1;
            }
        ).length > 0;
    }


    public getAbsoluteUrl(): string {
        let result = this.url;
        if (!/^http[s]?:\/\//.test(result)) {
            result = 'https://' + result;
        }

        return result;
    }

    addLabel(label: Label): void {
        this.labels.push(label);
    }

    getActiveLabels(): Label[] {
        return this.labels.filter(label => label.active);
    }

    public setLabelsFromRaw(rawLabels: any[]): void {
        // Convert the array of raw label data into an array of instances of Label, then call the setter.
        let actualLabels = rawLabels.map(label => {
            return new Label(label.id, label.name);
        });
        this.setLabels(actualLabels);
    }

    public setLabels(labels: Label[]): void {
        // Take the existing set of instances of Label, and replace this bookmark's existing set.
        this.labels = labels;
    }

    public getLabelsForTooltip(): string {
        // We need a CR-separated list of labels for this bookmark.
        if (this.labels.length == 0) {
            return '';
        }

        return "\n\nLabels:\n  "+this.labels.map(label => label.getName()).join("\n  ");
    }

    public hasLabel(label: Label): boolean {
        for (let labelIndex in this.labels) {
            if (this.labels[labelIndex] === label) {
                return true;
            }
        }

        return false;
    }
}