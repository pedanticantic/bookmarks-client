import { Node } from "./node";
import {AbstractElement} from "./abstractElement";

export class Cell extends AbstractElement {
    public static readonly NODE_TYPE = 'cell';

    id: number;
    nodeId: number;
    // Other properties TBC

    public static makeCell(entity: {id: number, nodeId: number, created_at: string, updated_at: string}, parentNode: Node) {
        let cell = new Cell();

        cell.id = entity.id;
        cell.nodeId = entity.nodeId;
        // grid.heading = entity.heading; @TODO: Properties TBA

        cell.parentNode = parentNode;

        return cell;
    }

    filterMatches(filter: string): boolean {
        return false; // There is nothing to filter a cell on.
    }
}
