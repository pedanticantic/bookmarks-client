import { Page } from "../components/pages/page";
import { Panel } from "./panel";
import { Bookmark } from "./bookmark";
import { Section } from "./section";
import { Grid } from "./grid";
import { Row } from "./row";
import { Cell } from "./cell";
import { Label } from "./label";
import { User } from "./user";
import {AbstractElement} from "./abstractElement";

export class Node {
    // Properties of the node itself.
    id: number;
    parentNodeId: number;
    userId: number;
    nodeType: string;
    sequenceNumber: number;

    // The owner of the node.
    owner: User;

    // The associated entities.
    page: Page;
    panel: Panel;
    bookmark: Bookmark;
    section: Section;
    grid: Grid;
    row: Row;
    cell: Cell;

    // References to other, related, nodes.
    childNodes: Node[] = [];

    // Which groups this node is shared with.
    private inGroupIds: number[] = [];
    private parentIsEmbedded: boolean;
    private isEmbedded: boolean;

    // Whether it is visible, based on the filter.
    private visible: boolean = true;

    public isA(nodeType: string): boolean {
        return this.nodeType == nodeType;
    }

    public getTooltip(): string {
        switch (this.nodeType) {
            case Bookmark.NODE_TYPE:
                return this.bookmark.url+this.bookmark.getLabelsForTooltip();
            case Section.NODE_TYPE:
                return '';
            case Cell.NODE_TYPE:
                return this.hasChildren(Bookmark.NODE_TYPE) ? this.childNodes[0].getTooltip() : '';
        }
        return '';
    }

    public static makeNode(nodeData) {
        let node = new Node();

        node.id = nodeData.id;
        node.parentNodeId = nodeData.parentNodeId;
        node.userId = nodeData.userId;
        node.nodeType = nodeData.nodeType;
        node.sequenceNumber = nodeData.sequenceNumber;

        if (nodeData.hasOwnProperty('owner')) {
            node.owner = User.makeUser(nodeData.owner);
        }

        // Set up the entity.
        // @TODO: We should check that nodeData actually has the appropriate property.
        switch (node.nodeType) {
            case Page.NODE_TYPE:
                node.page = Page.makePage(nodeData.page, node);
                break;
            case Panel.NODE_TYPE:
                node.panel = Panel.makePanel(nodeData.panel, node);
                break;
            case Bookmark.NODE_TYPE:
                node.bookmark = Bookmark.makeBookmark(nodeData.bookmark, node);
                break;
            case Section.NODE_TYPE:
                node.section = Section.makeSection(nodeData.section, node);
                break;
            case Grid.NODE_TYPE:
                node.grid = Grid.makeGrid(nodeData.grid, node);
                break;
            case Row.NODE_TYPE:
                node.row = Row.makeRow(nodeData.row, node);
                break;
            case Cell.NODE_TYPE:
                node.cell = Cell.makeCell(nodeData.cell, node);
                break;
            default:
                alert('Node of type '+node.nodeType+' has not been implemented yet');
                break;
        }

        // Build the list of groups this node is shared with.
        if (nodeData.hasOwnProperty('node_groups')) {
            nodeData.node_groups.forEach(nodeGroup => node.addToGroup(nodeGroup.groupId));
        }

        // Handle the child nodes.
        for (let childIndex in nodeData.child_nodes) {
            if (nodeData.child_nodes.hasOwnProperty(childIndex)) {
                node.addChild(
                    Node.makeNode(
                        nodeData.child_nodes[childIndex]
                    )
                );
            }
        }
        // Make sure they're sorted by sequence number.
        Node.sortBySeq(node.childNodes);

        return node;
    }

    // Method to accept an array of nodes, and sort them by sequence number.
    // It doesn't touch any of the nodes' child nodes.
    public static sortBySeq(nodes: Node[]) {
        nodes.sort(function (node1: Node, node2: Node) {
            return Math.sign(node1.sequenceNumber - node2.sequenceNumber);
        });
    }

    public hasChildren(ofType?: string): boolean {
        let children = this.childNodes;
        if (ofType != undefined) {
            children = children.filter(function (element: Node) {
                return element.nodeType == ofType;
            })
        }

        return children.length > 0;
    }

    public clearChildren(): void {
        this.childNodes = [];
    }

    public addChild(newChild: Node): void {
        this.childNodes.push(newChild);
    }

    public replaceChildren(newChildren: Node[]): void {
        this.childNodes = newChildren;
    }

    public openBookmark(requireManualOpen: Bookmark[]): Bookmark[] {
        let windowHandle = window.open(this.bookmark.getAbsoluteUrl());
        if (windowHandle == null) {
            // If this is the first window open to fail, pop up an alert, the real purpose of which
            // is to put focus back on the parent window/tab.
            if (requireManualOpen.length == 0) {
                alert('Your browser does not support opening multiple browsers with a single click');
            }
            requireManualOpen.unshift(this.bookmark);
        }

        return requireManualOpen;
    }

    public openBookmarksByLabel(label: Label, requireManualOpen?: Bookmark[]): Bookmark[] {
        requireManualOpen = requireManualOpen || [];
        // Traverse this node, and every time we come across a bookmark, see if it has the given label.
        // If so, open the bookmark in a new window (or tab).
        if (this.nodeType == Bookmark.NODE_TYPE && this.bookmark.hasLabel(label)) {
            requireManualOpen = this.openBookmark(requireManualOpen);
        }
        // We need to loop in reverse order because my browser seems to open each one in a new tab *before*
        // the previous one.
        for (let childIndex = this.childNodes.length-1; childIndex >= 0; childIndex--) {
            requireManualOpen = this.childNodes[childIndex].openBookmarksByLabel(label, requireManualOpen);
        }

        return requireManualOpen;
    }

    public refreshChildren(newNodeData: Node): void {
        // Recursively traverse the node hierarchy until we find the node whose id matches the node passed in.
        // When we find it, replace its children with the children from the node passed in.
        // @TODO: Make this slightly more efficient by returning up the call stack as soon as we find the node.
        if (this.id == newNodeData.id) {
            this.replaceChildren(newNodeData.childNodes);
        } else {
            for (let childIndex = this.childNodes.length-1; childIndex >= 0; childIndex--) {
                this.childNodes[childIndex].refreshChildren(newNodeData);
            }
        }
    }

    public getDisplayName(): string {
        let result = 'Unknown';
        switch (this.nodeType) {
            case Page.NODE_TYPE:
                result = this.page.name;
                break;
            case Panel.NODE_TYPE:
                result = this.panel.title;
                break;
            case Bookmark.NODE_TYPE:
                result = this.bookmark.label;
                break;
            case Section.NODE_TYPE:
                result = this.section.heading;
                break;
            case Grid.NODE_TYPE:
                result = 'Grid';
                break;
            case Row.NODE_TYPE:
                result = 'Row';
                break;
            case Cell.NODE_TYPE:
                result = 'Cell';
                break;
        }

        return result;
    }

    public addToGroup(groupId: number) {
        this.removeFromGroup(groupId); // Just make sure it's not in there, otherwise we could end up with a duplicate.
        this.inGroupIds.push(groupId);
    }

    public removeFromGroup(groupId: number) {
        // Keep any group that's not the group passed in.
        this.inGroupIds = this.inGroupIds.filter((inGroupId: number) => inGroupId != groupId);
    }

    public isSharedWithGroup(groupId: number): boolean {
        return this.inGroupIds.filter((inGroupId: number) => inGroupId == groupId).length > 0;
    }

    public hasGroups(): boolean {
        return this.inGroupIds.length > 0;
    }

    public getManageGroupTitle(): string {
        let numGroups = this.inGroupIds.length;
        return 'Manage groups - currently in '+numGroups+' group'+(numGroups == 1 ? '' : 's');
    }

    /**
     * Method to return the id of this node merged with the result of calling the method recursively for each
     * child node. When called at the parent level, the result will be all the node ids in the page.
     *
     * @returns {int[]}
     */
    public getAllNodeIds(): number[] {
        let result = [this.id];
        this.childNodes.forEach((childNode: Node) => {
            result = result.concat(childNode.getAllNodeIds());
        });

        return result;
    }

    public getDescendentNodeById(nodeId: number): Node|null {
        let result: Node|null;
        this.childNodes.forEach((childNode: Node) => {
          if (!result) {
            if (childNode.id === nodeId) {
              result = childNode;
            } else {
              result = childNode.getDescendentNodeById(nodeId);
            }
          }
        });

        return result;
    }

    /**
     * Method to traverse the node tree. The embedded flag is no by default, but if a node is not owned by the
     * logged in user, it, and all its descendants are deemed to be embedded.
     */
    public determineEmbeddedFlags(userId: number, parentIsEmbedded?: boolean) {
        this.parentIsEmbedded = parentIsEmbedded == undefined ? false : parentIsEmbedded;
        this.isEmbedded = this.userId != userId || this.parentIsEmbedded;

        // Now do the children.
        this.childNodes.forEach((node: Node) => {
            node.determineEmbeddedFlags(userId, this.isEmbedded);
        });
    }

    public canEdit(): boolean {
        return !this.isEmbedded;
    }

    public canManageGroups(): boolean {
        return !this.isEmbedded;
    }

    public canAddChild(): boolean {
        return !this.isEmbedded;
    }

    public isFirstEmbedded(): boolean {
        // Is this the first node in an embedded tree?
        return !this.parentIsEmbedded && this.isEmbedded;
    }

    public isDraggable(): boolean {
        return !this.parentIsEmbedded;
    }

    public isDroppable(): boolean {
        return !this.isEmbedded;
    }

    public draggableData(): object {
        return {id: this.id};
    }

    /**
     * Method that says whether this node can be deleted. It's quite complex. If its parent is embedded,
     * the user cannot delete it; if its parent is not embedded but it itself is embedded, then they can.
     * Otherwise (neither parent nor itself is embedded), it is subject to the specific rules for that type
     * of object.
     * @returns {boolean}
     */
    public canDelete(): boolean {
        if (this.parentIsEmbedded) {
            return false; // An ancestor is embedded, so it cannot be deleted/removed in isolation.
        }
        if (this.isEmbedded) {
            return true; // This is a "top-level" node in an embedded tree, so it can be removed.
        }
        // If it is in any groups, it can't be deleted.
        if (this.hasGroups()) {
            return false;
        }
        // It now depends on the type of node.
        switch (this.nodeType) {
            case Page.NODE_TYPE:
            case Panel.NODE_TYPE:
            case Section.NODE_TYPE:
                return this.childNodes.length == 0; // Must not have any children.
            case Bookmark.NODE_TYPE:
            case Row.NODE_TYPE: // This is a slight fudge because the template checks there are more than 1 rows in the grid.
            case Grid.NODE_TYPE:
                return true; // Bookmarks never have any child nodes, so can always be deleted; grids and rows can be deleted.
            default:
                return false;
        }
    }

    public applyFilter(filter: string, parentIsVisible: boolean): boolean {
        // This element is visible if any of these are true:
        //  the filter is empty
        //  the filter matches this node
        //  any descendant node matches the filter
        //  any ancestor node matches the filter

        // Check whether this node is deemed to match the filter.
        this.visible = true;
        let matchesAtThisLevelOrAbove = parentIsVisible;
        if (!(filter == '' || parentIsVisible)) {
            let element:AbstractElement = null;
            switch (this.nodeType) {
                case Page.NODE_TYPE:
                    element = this.page;
                    break;
                case Panel.NODE_TYPE:
                    element = this.panel;
                    break;
                case Bookmark.NODE_TYPE:
                    element = this.bookmark;
                    break;
                case Section.NODE_TYPE:
                    element = this.section;
                    break;
                case Grid.NODE_TYPE:
                    element = this.grid;
                    break;
                case Row.NODE_TYPE:
                    element = this.row;
                    break;
                case Cell.NODE_TYPE:
                    element = this.cell;
                    break;
            }
            matchesAtThisLevelOrAbove = element.filterMatches(filter);
            this.visible = matchesAtThisLevelOrAbove;
        }

        this.childNodes.forEach((childNode: Node) => {
            // Be careful about combining these lines, applyFilter() must be executed even if
            // this.visible is true - the interpreter might take short-cuts and not execute the former.
            let descendantsVisible = childNode.applyFilter(filter, matchesAtThisLevelOrAbove);
            this.visible = this.visible || descendantsVisible;
        });

        // Special case: if a row is visible, make sure all its descendants are visible.
        // This keeps columns intact (might be confusing for the user, otherwise).
        if (this.visible && this.nodeType == Row.NODE_TYPE) {
            this.makeChildrenVisible();
        }

        return this.visible;
    }

    public getVisibleChildNodes(): Node[] {
        return this.childNodes.filter(
            function (element: Node) {
                return element.visible;
            }
        );
    }

    public makeChildrenVisible(): void {
        this.childNodes.forEach((childNode: Node) => {
            childNode.visible = true;
            childNode.makeChildrenVisible();
        });
    }
}
