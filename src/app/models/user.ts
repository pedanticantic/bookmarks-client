export class User {
    id: number;
    givenName: string;
    familyName: string;
    email: string;

    public static makeUser(userData): User {
        let user = new User();

        user.id = userData.id;
        user.givenName = userData.name;
        user.familyName = userData.familyName;
        user.email = userData.email;

        return user;
    }

    public getFullName(): string {
        return this.givenName+' '+this.familyName;
    }
}