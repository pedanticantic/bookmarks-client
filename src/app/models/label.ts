export class Label {
    active: boolean = true;
    private className: string = 'bullet1';
    private isHighlighted: boolean = false;

    constructor(
        private id: number,
        private name: string
    ) {}

    public getId(): number {
        return this.id;
    }

    public getName(): string {
        return this.name;
    }

    public getClassName(): string {
        return this.className+(this.isHighlighted ? ' highlight' : '');
    }

    public isNew(): boolean {
        return this.id == null;
    }

    public remove(): void {
        this.active = false;
    }

    public setClassName(className: string): void {
        this.className = className;
    }

    public highlight(): void {
        this.isHighlighted = true;
    }

    public unHighlight(): void {
        this.isHighlighted = false;
    }
}