import { Node } from "./node";
import {AbstractElement} from "./abstractElement";

export class Row extends AbstractElement {
    public static readonly NODE_TYPE = 'row';

    id: number;
    nodeId: number;
    // Other properties TBC

    public static makeRow(entity, parentNode: Node) {
        let row = new Row();

        row.id = entity.id;
        row.nodeId = entity.nodeId;
        // grid.heading = entity.heading; @TODO: Properties TBA

        row.parentNode = parentNode;

        return row;
    }

    filterMatches(filter: string): boolean {
        return false; // There is nothing to filter a row on.
    }
}