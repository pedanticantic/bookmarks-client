import { Node } from "./node";
import {AbstractElement} from "./abstractElement";

export class Section extends AbstractElement {
    public static readonly NODE_TYPE = 'section';

    id: number;
    nodeId: number;
    heading: string = '';

    public static makeSection(entity, parentNode: Node) {
        let section = new Section();

        section.id = entity.id;
        section.nodeId = entity.nodeId;
        section.heading = entity.heading;

        section.parentNode = parentNode;

        return section;
    }

    filterMatches(filter: string): boolean {
        return this.heading != null && this.heading.toLowerCase().indexOf(filter) != -1;
    }
}