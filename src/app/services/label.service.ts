import {Injectable} from '@angular/core';
import {Observable, throwError} from 'rxjs';
// import 'rxjs/add/operator/map';
import {ApiCallerService} from './api-caller.service';
import {Label} from "../models/label";
import {Bookmark} from "../models/bookmark";
import {Node} from "../models/node";
import {Page} from "../components/pages/page";
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Injectable()
export class LabelService {
    labels: Label[] = [];

    constructor(
        private apiCallerService: ApiCallerService) {
    }

    getLabels(bookmark?: Bookmark): void {
        // Get Labels from API.
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of labels.
          this.labels = response.map(item => {
            return new Label(item['id'], item['name']);
          });
          this.assignClasses();
          if (bookmark && bookmark instanceof Bookmark) {
            this.mapBookmarkLabels(bookmark);
          }
        };

        this.apiCallerService.callApi('get', 'labels', true, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        });
    }

    private assignClasses() {
        for (let labelIndex in this.labels) {
            this.labels[labelIndex].setClassName('bullet'+(1+parseInt(labelIndex) % 8));
        }
    }

    /**
     * This method goes through the entire page, extracting one instance of each label into a master
     * list, assigning the appropriate instances back to the bookmarks, and then calls the method to
     * assign the appropriate class name to those labels.
     *
     * @param {Node} node
     */
    public extractLabels(node: Node): void {
        let isTopLevel = false;
        switch (node.nodeType) {
            case Page.NODE_TYPE:
                isTopLevel = true;
                // Reset the labels if this is the top-level node.
                this.labels = [];
                break;
            case Bookmark.NODE_TYPE:
                // Actually extract all the labels.
                // There is a very subtle issue here - if the same label is against multiple bookmarks,
                // each bookmark will have a separate _instance_ of it, so as we pick out the labels,
                // we must assign the instance of the label we're keeping back over the label in the
                // bookmark. This means that if we set "highlight" to true in one of the labels in a
                // bookmark, all the other labels in other bookmarks that are the same label, also get
                // their flag set (because they are all the same instance of the label).
                node.bookmark.labels.forEach(
                    (label: Label, index: number) => {
                        // Only add to the list if we've never seen it before.
                        if (typeof this.labels[label.getId()] == 'undefined') {
                            this.labels[label.getId()] = label;
                        }
                        // Assign the instance of the label from the "official" list back over the top
                        // of this label (so that a single instance of each label is assigned to all
                        // the relevant bookmarks).
                        node.bookmark.labels[index] = this.labels[label.getId()];
                    }
                );
        }
        // Call ourself recursively for all our children.
        node.childNodes.forEach(
            (childNode: Node) => {
                this.extractLabels(childNode);
            }
        );
        // If this is the top level, then we've finished.
        if (isTopLevel) {
            // Re-index, and then assign classes.
            let tempLabels = [];
            this.labels.forEach(
                (label: Label) => {
                    tempLabels.push(label);
                }
            );
            this.labels = tempLabels;
            this.assignClasses();
        }
    }

    private mapBookmarkLabels(bookmark: Bookmark) {
        // Loop through the labels, making sure each one is an appropriate instance of Label.
        for (let labelIndex in bookmark.labels) {
            let thisLabel = bookmark.labels[labelIndex];
            if (thisLabel.getId()) {
                thisLabel = this.getById(thisLabel.getId());
            } else {
                if (thisLabel.getName()) {
                    thisLabel = this.getByName(thisLabel.getName());
                }
            }
            bookmark.labels[labelIndex] = thisLabel;
        }
    }

    private getById(id: number) {
        for (let labelIndex in this.labels) {
            if (this.labels[labelIndex].getId() == id) {
                return this.labels[labelIndex];
            }
        }

        // @TODO: I guess this needs to error.
        return null;
    }

    private getByName(name: string) {
        for (let labelIndex in this.labels) {
            if (this.labels[labelIndex].getName() == name) {
                return this.labels[labelIndex];
            }
        }

      // @TODO: I guess this needs to error.
      return null;
    }

    public getPageLabels(): Label[] {
        return this.labels;
    }
}
