import {Injectable} from "@angular/core";
import {Node} from "../models/node";

@Injectable()
export class FilterService {
    public filter: string = '';

    public clear(pageNode: Node): void {
        this.filter = '';
        this.handleFilterChange(pageNode);
    }

    public handleFilterChange(pageNode: Node): void {
        pageNode.applyFilter(this.filter.toLowerCase(), false);
    }

    public isFiltered(): boolean {
        return this.filter != '';
    }
}
