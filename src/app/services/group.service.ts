import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
// import 'rxjs/add/operator/map';
import { ApiCallerService } from './api-caller.service';
import { GroupMember } from "../models/groupMember";
import { Group } from "../models/group";
import { GroupNode } from "../models/GroupNode";
import {HttpResponse} from '@angular/common/http';

@Injectable()
export class GroupService {
    private groupMemberList: GroupMember[] = [];
    private sharedNodesList: GroupNode[] = [];
    constructor(
        private apiCallerService: ApiCallerService) {
    }

    loadUserGroups(callback?: Function): Observable<GroupMember[]> {
        // Get groups that the logged in user is in from API.
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of groups (fully hydrated), then sort the list.
            let groupNodes = response.map(item => {
                let groupMemberData = {
                    group: {
                        id: item['groupId'],
                        name: item['groupName'],
                        createdByUserId: item['createdByUserId']
                    },
                    groupMember: {
                        role: item['role'],
                        status: item['status']
                    },
                    user: {
                        id: item['userId'],
                        givenName: item['userGivenName'],
                        familyName: item['userFamilyName']
                    }
                };

                return GroupMember.makeGroupMember(groupMemberData);
            });
            GroupMember.sortByGroupName(groupNodes);
            this.groupMemberList = groupNodes;

            if (callback) {
                callback(groupNodes);
            }
        };

        return this.apiCallerService.callApi('get', 'user/groups', true, null, mapCallback);
        // @TODO: Handle errors
        //     .catch(this.handleError)
    }

    getUserGroups(): GroupMember[] {
        return this.groupMemberList;
    }

    getGroup(groupId: number): Observable<Group> {
        // Get group from API.
        let mapCallback = (response: object) => {
            // Convert the raw data to a group.
            return Group.makeGroup(response);
        };

        return this.apiCallerService.callApi('get', 'groups/'+groupId, true, null, mapCallback);
        // @TODO: Handle errors.
        //     .catch(this.handleError);
    }

    getGroupMembers(groupId: number): Observable<GroupMember[]> {
        // Get group members from API.
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of group members (fully hydrated), then sort the list.
            let groupMembers = response.map(item => {
                let groupMemberData = {
                    group: item['group'],
                    groupMember: item,
                    user: item['user']
                };
                return GroupMember.makeGroupMember(groupMemberData);
            });
            GroupMember.sortByGroupName(groupMembers);

            return groupMembers;
        };

        return this.apiCallerService.callApi('get', 'groups/'+groupId+'/members', true, null, mapCallback);
        // @TODO: Handle errors.
        //     .catch(this.handleError);
    }

    getGroupNodes(groupId: number): Observable<GroupNode[]> {
        // Get group nodes from API.
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of group nodes (fully hydrated), then sort the list.
            let groupNodes = response.map(item => {
                let groupNodeData = {
                    group: item['group'],
                    node: item['node']
                };
                return GroupNode.makeGroupNode(groupNodeData);
            });
            GroupNode.sortByOwnerAndType(groupNodes);

            return groupNodes;
        };

        return this.apiCallerService.callApi('get', 'groups/'+groupId+'/nodes', true, null, mapCallback);
        // @TODO: Handle errors.
        //     .catch(this.handleError);
    }

    loadSharedNodes(callback?: Function): Observable<GroupNode[]> {
        // Ask the API for all nodes shared with the user via their groups
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of groups, each one containing details of the nodes in that group.
            let groupNodes = response.map(item => {
                let groupNodeData = {
                    group: item['group'],
                    node: item['node']
                    // @TODO: Flag to say whether the node is shared in this page.
                };
                return GroupNode.makeGroupNode(groupNodeData);
            });
            // @TODO: This worked in the old version, but doesn't work now
            // GroupMember.sortByGroupName(groupNodes);
            this.sharedNodesList = groupNodes;

            if (callback) {
              callback(groupNodes);
            }
        };

        return this.apiCallerService.callApi('get', 'nodes/sharedWith', true, null, mapCallback);
        // @TODO: Handle errors.
        //     .catch(this.handleError)
    }

    public getSharedNodesList(): GroupNode[] {
        return this.sharedNodesList;
    }

    public getSharedNodesOfType(type: string): GroupNode[] {
        return this.sharedNodesList.filter((groupNode: GroupNode) => groupNode.node.nodeType == type);
    }

    // @TODO: Sort this out.
    // private handleError(error: HttpResponse<any>) {
    //     return Observable.throw(error.statusText);
    // }
}
