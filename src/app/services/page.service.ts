import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs';
// import 'rxjs/add/operator/map';
import { Node } from '../models/node';
import { ApiCallerService } from './api-caller.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Injectable()
export class PageService {
    constructor(
        private apiCallerService: ApiCallerService) {
    }

    getPages(callback: Function): Observable<Node[]> {
        // Get pages from API.
        let mapCallback = (response: object[]) => {
            // Convert the raw data to an array of page nodes (fully hydrated), then make sure the top-level nodes are sorted.
            let pageNodes = response.map((item): Node => {
                return Node.makeNode(item);
            });
            Node.sortBySeq(pageNodes);

            callback(pageNodes);
        };

        return this.apiCallerService.callApi('get', 'pages', true, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        });
    }

    getPage(pageId: number, callback: Function): Observable<Node> {
        // Get page from API.
        let mapCallback = (response: HttpResponse<any>) => {
            // Convert the raw data to a page node (fully hydrated).
            callback(Node.makeNode(response));
        };

        return this.apiCallerService.callApi('get', 'pages/'+pageId, true, null, mapCallback, (error: HttpErrorResponse) => {
          throwError(() => new Error(error.statusText));
        });
    }

    getPageByToken(pageToken: string, callback: Function): Observable<Node> {
        // Get page from API.
        let mapCallback = (response: HttpResponse<any>) => {
        // Convert the raw data to a page node (fully hydrated).
            callback(Node.makeNode(response));
        };

        return this.apiCallerService.callApi('get', 'publish/page/'+pageToken, false, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        });
    }

    getPageToken(pageId: number, callback: Function): Observable<any> {
        // Get page from API.
        let mapCallback = (response: HttpResponse<any>) => {
            // Just convert the raw data to JSON.
          callback(response);
        };

        return this.apiCallerService.callApi('get', 'publish/page/'+pageId+'/token', true, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        });
    }
}
