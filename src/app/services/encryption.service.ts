import {Injectable} from "@angular/core";
import {sha256} from 'js-sha256';

@Injectable()
export class EncryptionService {
    private generatePasswordResetCheck(version: number, email: string)
    {
        return sha256.hex(JSON.stringify({version: version, email: email, salt: 'somesalt'}));
    }

    public generatePasswordResetParameters(email: string) {
        return '?email='+email+'&v=1&check='+this.generatePasswordResetCheck(1, email);
    }

    public validatePasswordReset(email: string, version: number, check: string) {
        return check == this.generatePasswordResetCheck(version, email);
    }
}
