import {Injectable} from "@angular/core";
import {Node} from "../models/node";
import {NodeHierarchyList} from "../valueObjects/NodeHierarchyList";
import {ApiCallerService} from "./api-caller.service";
import {NodeHierarchy} from "../valueObjects/NodeHierarchy";
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';

@Injectable()
export class NodeService {
    private nodeHierarchies: NodeHierarchyList[] = [];

    constructor(
        private apiCallerService: ApiCallerService
    ) {}

    private loadNodeHierarchies(node: Node): Observable<NodeHierarchyList> {
        // Get hierarchies for the given node.
        let mapCallback = (response: HttpResponse<any>) => {
            function processNode(nodeData): NodeHierarchy[] {
                let hierarchies: NodeHierarchy[] = [];
                if (nodeData.parents.length == 0) {
                    let newHierarchy = new NodeHierarchy();
                    hierarchies.push(newHierarchy);
                } else {
                    nodeData.parents.forEach(function (parentNode) {
                        hierarchies = hierarchies.concat(processNode(parentNode));
                    });
                }
                hierarchies.forEach(function(nodeHierarchy: NodeHierarchy) {
                    nodeHierarchy.append(Node.makeNode(nodeData.node));
                });

                return hierarchies;
            }

            // Traverse the response (an inverted tree, starting from the given node, going up), and for each leave
            // node, build a NodeHierarchy object, and then return them as a List object.
            return new NodeHierarchyList(processNode(response));
        };

        return this.apiCallerService.callApi('get', 'nodes/'+node.id+'/hierarchies', true, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        })
    }

    public getHierarchies(node: Node): NodeHierarchyList {
        if (!(node.id in this.nodeHierarchies)) {
            this.nodeHierarchies[node.id] = new NodeHierarchyList([]);
            this.loadNodeHierarchies(node).subscribe((nodeHierarchies: NodeHierarchyList) => {
                this.nodeHierarchies[node.id] = nodeHierarchies;
            });
        }

        return this.nodeHierarchies[node.id];
    }

    public clearNodeHierarchies() {
        this.nodeHierarchies = [];
    }
}
