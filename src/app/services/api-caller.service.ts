import { Injectable } from '@angular/core';
import { AuthenticationService } from './authentication.service';
import { ApiConfigService } from "./api-config.service";
// import 'rxjs/Rx';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import {throwError} from 'rxjs';

@Injectable()
export class ApiCallerService {
    constructor(
        private http: HttpClient,
        private authenticationService: AuthenticationService,
        private apiConfigService: ApiConfigService
    ) {
    }

    public callApi(verb: string, path: string, authenticate: boolean, postData?,  mapCallback?: Function,  errorCallback?: Function): Observable<any> {
        let options = {};
        if (authenticate) {
            let headers = new HttpHeaders({
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + this.authenticationService.token
            });
            options = { headers: headers};
        }
        mapCallback = mapCallback || function defaultMap(response: HttpResponse<any>) { return response; };
        let url = this.apiConfigService.getHost()+path;
console.log('callApi(): url:', url);
        let result = null;
console.log('callApi(): verb:', verb);
        switch (verb) {
            case 'get':
                result = this.http.get(url, options);
                break;
            case 'post':
                result = this.http.post(url, postData, options);
                break;
            case 'put':
                result = this.http.put(url, postData, options);
                break;
            case 'delete':
                result = this.http.delete(url, options);
                break;
            default:
                alert('Unknown verb: '+verb);
                break;
        }
console.log('callApi(): result:', result);
        return result
            .subscribe({
                next: (response: HttpResponse<any>) => {
console.log('callApi(): response in subscribe:', response);
                return mapCallback(response);
            },
            error: (error: HttpErrorResponse) => {
console.log('There was an error:', error);
                if (errorCallback) {
console.log('There is an error callback');
                    return errorCallback(error);
                }
console.log('No error callback');

                return throwError(() => new Error(error.statusText));
            }
        });
    }
}
