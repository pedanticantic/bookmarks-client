import { Injectable } from '@angular/core';
import {Observable, Subscription, tap} from 'rxjs';
import { Router } from '@angular/router';
// import 'rxjs/add/operator/map';
import { ApiConfigService } from "./api-config.service";
import { AUTH_CONFIG } from "../config/auth0-variables";
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpResponse} from '@angular/common/http';
import {UserToken} from '../models/userToken';

@Injectable()
export class AuthenticationService {
    public token: string|null;
    public userName: string;
    private userId: number;
    private name: string;
    private familyName: string;

    constructor(
        private http: HttpClient,
        private router: Router,
        private apiConfigService: ApiConfigService,
    ) {
        // set token if saved in local storage
        let currentUser: UserToken|null = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
        this.userName = currentUser && currentUser.username;
        this.getLoggedInUserDetails();
    }

    login(username: string, password: string, callback: Function): void {
        let headers = new HttpHeaders({'Content-Type': 'application/json'});
        let options = {headers: headers};
        this.http.post(
            this.apiConfigService.getHost()+'oauth/token',
            JSON.stringify({
                grant_type: 'password',
                client_id: AUTH_CONFIG.CLIENT_ID,
                client_secret: AUTH_CONFIG.CLIENT_SECRET,
                username: username,
                password: password,
                scope: AUTH_CONFIG.SCOPE
            }),
            options,
        ).subscribe({
          next: (response: {access_token: string, expires_in: number, refresh_token: string, token_type: string}) => {
              let result: boolean;
              // login successful if there's a jwt token in the response
              let token = response.access_token;
              if (token) {
                  // set token property
                  this.token = token;

                  // store username and jwt token in local storage to keep user logged in between page refreshes
                  localStorage.setItem('currentUser', JSON.stringify({ username: username, token: token }));
                  this.userName = username;

                  this.getLoggedInUserDetails();

                  // return true to indicate successful login
                  result = true;
              } else {
                  // return false to indicate failed login
                  result = false;
              }

              callback(result);
          },
          error: (error: HttpErrorResponse) => {
console.log('login error: ', error);
              callback(false);
          }
        });
    }

    logout(redirectToHome: boolean): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
        if (redirectToHome) {
            this.router.navigate(['/']);
        }
    }

    authenticated(): boolean {
        return !!this.token;
    }

    getLoggedInUserDetails(): void {
        // After successful login, or on startup when the user is already logged in, ask the API for the
        // secondary details of the logged-in user - user id, name, etc.
        // @TODO: Need to be able to find a way of use ApiCallerService without ending up with a circular dependency.
        let headers = new HttpHeaders({
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.token
        });
        let options = {headers: headers};
        let url = this.apiConfigService.getHost()+'user/loggedInUser';
        this.http.get(url, options)
            .subscribe({
                next: (response: {created_at: string, email: string, familyName: string, id: number, name: string, updated_at: string}) => {
                    this.userId = response.id;
                    this.name = response.name;
                    this.familyName = response.familyName;
                },
                error: (error: HttpErrorResponse) => {
                    console.log('Error getting logged-in user details: ', error);
                }
            });
    }

    getDisplayName(): string {
        return this.name ? this.name+' '+this.familyName : this.userName;
    }

    getUserId(): number {
        return this.userId;
    }

    getGivenName(): string {
        return this.name;
    }

    getFamilyName(): string {
        return this.familyName;
    }

    getEmail(): string {
        return this.userName;
    }

    updateUser(newDetails: {email: string, name: string, familyName: string}) {
        // Email, given name and family name might have been updated.
        if (newDetails.hasOwnProperty('email')) {
            this.userName = newDetails.email;
            localStorage.setItem('currentUser', JSON.stringify({ username: this.userName, token: this.token }));
        }
        if (newDetails.hasOwnProperty('name')) {
            this.name = newDetails.name;
        }
        if (newDetails.hasOwnProperty('familyName')) {
            this.familyName = newDetails.familyName;
        }
    }
}
