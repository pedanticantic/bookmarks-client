import { Injectable } from '@angular/core';
import { AUTH_CONFIG } from '../config/auth0-variables';

@Injectable()
export class ApiConfigService {
    public getHost(): string {
        return AUTH_CONFIG.CLIENT_HOST;
    }
}
