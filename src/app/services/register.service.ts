import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiCallerService } from './api-caller.service';

@Injectable()
export class RegisterService {

    constructor(private apiCallerService: ApiCallerService) {}

    register(name: string, familyName: string, email: string, password, callback: Function): Observable<boolean> {
        // Call the API to create the user.
        // The API requires a "password confirmation" field, but I'm not bothering with it, so just
        // copy the password into that field, and it'll be happy.
        let userData = {
            name: name,
            familyName: familyName,
            email: email,
            password: password,
            password_confirmation: password,
        };
        let registerCallback = function registerCallback(response) {
            callback(response.hasOwnProperty('success') && response.success === true);
        };
        return this.apiCallerService.callApi('post', 'register', false, userData, registerCallback);
    }
}
