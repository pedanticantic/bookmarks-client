interface AuthConfig {
    CLIENT_ID: string;
    CLIENT_HOST: string;
    CLIENT_SECRET: string;
    // AUDIENCE: string;
    // REDIRECT: string;
    SCOPE: string;
}

export const AUTH_CONFIG: AuthConfig = {
    CLIENT_ID: '99',
    CLIENT_HOST: 'http://not.this.one/',
    CLIENT_SECRET: 'ABC123',
    // AUDIENCE: 'Your-audience',
    // REDIRECT: 'http://localhost:4200/callback',
    SCOPE: ''
};
