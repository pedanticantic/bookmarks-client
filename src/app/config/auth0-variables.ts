interface AuthConfig {
  CLIENT_ID: string;
  CLIENT_HOST: string;
  CLIENT_SECRET: string;
  // AUDIENCE: string;
  // REDIRECT: string;
  SCOPE: string;
}

// Dev
export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: '2',
  CLIENT_HOST: 'http://bookmarks-api.pedanticantic.local/',
  CLIENT_SECRET: 'ZxmSCVyhmv2WImDA1j6it3hdTVeOMBFBMkjXLV76',
  // AUDIENCE: 'Your-audience',
  // REDIRECT: 'http://localhost:4200/callback',
  SCOPE: ''
};

// Production
/*export const AUTH_CONFIG: AuthConfig = {
  CLIENT_ID: '4',
  CLIENT_HOST: 'https://api.bookmarks.pedanticantic.click/',
  CLIENT_SECRET: 'm41ZA6vkCWlTGfmEVHr7NEdSdKHrR5b8cIcu5aGw',
  // AUDIENCE: 'Bookmarks',
  // REDIRECT: 'http://localhost:4200/callback',
  SCOPE: ''
};*/
