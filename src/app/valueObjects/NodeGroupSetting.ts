export class NodeGroupSetting {
    constructor(
        private groupId: number,
        private groupName: string,
        public isShared: boolean
    ) {}

    public getGroupId(): number {
        return this.groupId;
    }

    public getGroupName(): string {
        return this.groupName;
    }

    public isSharedWithGroup(): boolean {
        return this.isShared;
    }
}