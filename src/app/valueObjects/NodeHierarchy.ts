import {Node} from "../models/node";

export class NodeHierarchy {
    private nodeStack: Node[] = [];

    constructor() {}

    public append(node: Node): void {
        this.nodeStack.push(node);
    }

    public isEmbedded(): boolean {
        return this.getFirst().owner.id != this.getLast().owner.id;
    }

    private getFirst(): Node {
        return this.nodeStack[0];
    }

    private getLast(): Node {
        return this.nodeStack[this.nodeStack.length-1];
    }

    public getDisplayValue(): string {
        // Convert each element of the node stack into a description of that node, then join them with an
        // appropriate separator, and return the resulting string.
        return this.nodeStack.map((node: Node) => '['+node.nodeType+'] '+node.getDisplayName()).join(' -> ');
    }
}