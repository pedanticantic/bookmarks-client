import {NodeHierarchy} from "./NodeHierarchy";

export class NodeHierarchyList {
    constructor(
        private nodeHierarchy: NodeHierarchy[]
    ) {}

    public forOwner(): NodeHierarchy[] {
        return this.nodeHierarchy.filter(
            (hierarchy: NodeHierarchy) => !hierarchy.isEmbedded()
        );
    }

    public forOthers(): NodeHierarchy[] {
        return this.nodeHierarchy.filter(
            (hierarchy: NodeHierarchy) => hierarchy.isEmbedded()
        );
    }
}