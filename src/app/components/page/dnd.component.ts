import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {DndDropEvent, DndModule} from 'ngx-drag-drop';

@Component({
  templateUrl: 'dnd.component.html',
  styleUrls: ['./manage-page.component.css'],
  providers: [AuthenticationService],
  imports: [
    DndModule
  ]
})
export class DndComponent implements OnInit, OnDestroy {
  dragData: any = {
    data: 'foo',
    effectAllowed: 'all'
  };
  dzTypes: any = {
    dzOne: 'one',
    dzTwo: 'two',
    dzThree: 'three',
  };
  allowedTypes: any = {
    justOne: [this.dzTypes.one],
    oneAndTwo: [this.dzTypes.one, this.dzTypes.two],
  };
  ngOnDestroy(): void {
    // Do nothing.
  }

  ngOnInit(): void {
    // Do nothing.
  }

  public onDragoverGlenn(event: DragEvent) {
    // console.log('dragover:', event);
  }

  public onDropGlenn(event: DndDropEvent) {
    console.log('Something was dropped. event:', event);
  }
}
