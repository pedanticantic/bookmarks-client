import {Component, OnDestroy, OnInit} from "@angular/core";
import {Node} from "../../models/node";
import {PageService} from '../../services/page.service';
import {ActivatedRoute, Router, RouterLink} from "@angular/router";
import {ModalService} from "../../modal/modal.service";
import {Bookmark} from "../../models/bookmark";
import {Panel} from "../../models/panel";
import {Section} from "../../models/section";
import {Grid} from "../../models/grid";
import {Cell} from "../../models/cell";
import {ApiCallerService} from "../../services/api-caller.service";
import {Title} from '@angular/platform-browser';
import {LabelService} from "../../services/label.service";
import {Label} from "../../models/label";
import {GroupService} from "../../services/group.service";
import {NodeGroupSetting} from "../../valueObjects/NodeGroupSetting";
import {GroupNode} from "../../models/GroupNode";
import {AuthenticationService} from "../../services/authentication.service";
import {FilterService} from "../../services/filter.service";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faBars, faBookmark, faCaretSquareDown, faCaretSquareRight, faExternalLink, faEye, faEyeSlash, faFileText, faPencilSquare, faPlusSquare, faSpinner, faTrash, faUserGroup} from '@fortawesome/free-solid-svg-icons';
import {FormsModule} from '@angular/forms';
import {NgClass, NgForOf, NgIf} from '@angular/common';
import {ModalComponent} from '../../modal/modal.component';
import {DndModule} from 'ngx-drag-drop';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Component({
  templateUrl: 'manage-page.component.html',
  styleUrls: ['./manage-page.component.css'],
  providers: [PageService, LabelService, AuthenticationService],
  imports: [
    FaIconComponent,
    FormsModule,
    RouterLink,
    NgClass,
    ModalComponent,
    NgIf,
    DndModule,
    NgForOf
  ]
})
export class ManagePageComponent implements OnInit, OnDestroy {
    pageNode: Node;
    publishedToken = {
        isPublished: null,
        token: null
    };
    loading: boolean;

    // For adding and editing panels
    addEditPanelModalTitle: string = 'TBA';
    panelCopy: Panel = null;
    addEditPanelModalId: string = 'AddEditPanel';
    originalPanel: Panel = null;

    // For adding and editing bookmarks.
    addEditBookmarkModalId: string = 'AddEditBookmark';
    addEditBookmarkModalTitle: string = 'TBA';
    originalBookmark: Bookmark = null;
    bookmarkCopy: Bookmark = null;
    errorMessage: string = '';
    currentParentNode: Node = null;

    // For adding and editing sections.
    addEditSectionModalId: string = 'AddEditSection';
    addEditSectionModalTitle: string = 'TBA';
    originalSection: Section = null;
    sectionCopy: Section;

    // For adding grids.
    addGridModalId: string = 'AddGrid';
    addGridModalTitle: string = 'TBA';
    gridCopy: Grid;

    // For unpublishing a page
    unpublishPageModalId: string = 'unpublishPage';
    unpublishPageModalTitle: string = 'Confirm Unpublish';

    // For showing/searching for labels.
    labelSearch: string = '';

    // For dragging a panel to another page.
    movePanelBetweenPagesModalTitle: string = 'Move Panel to Another Page';
    movePanelBetweenPagesModalId: string = 'movePanelBetweenPages';
    movePanelBetweenPagesPageList: Node[] = [];
    movePanel: boolean = false;
    panelToMove: Node;
    selectedPageNodeId: any;

    // For deleting objects.
    deleteObjectModalTitle: string = 'TBC';
    deleteObjectNode: Node;
    parentOfDeletedNode: Node;
    deleteObjectModalId: string = 'DeleteObject';

    // For managing which groups a node is shared with.
    managingGroupNodes: boolean = false;
    manageGroupNodesModalTitle: string = '';
    manageGroupsForNode: Node = null;
    nodeGroupSettings: NodeGroupSetting[];

    // For displaying shared nodes.
    sharedNodeGroupings: any[];

    // @TODO: This should be in its own service.
    private draggingTargetTypes: string[] = [];

    constructor(
        private pageService: PageService,
        private titleService: Title,
        private route: ActivatedRoute,
        public modalService: ModalService,
        private apiCallerService: ApiCallerService,
        private router: Router,
        private labelService: LabelService,
        public groupService: GroupService,
        private authenticationService: AuthenticationService,
        public filterService: FilterService
    ) {
        this.sharedNodeGroupings = [
            {type: Panel.NODE_TYPE, desc: 'Panel'},
            {type: Section.NODE_TYPE, desc: 'Section'},
            {type: Grid.NODE_TYPE, desc: 'Grid'},
            {type: Bookmark.NODE_TYPE, desc: 'Bookmark'},
        ];
    }

    ngOnInit(): void {
        this.loading = true;
        const pageId: number = Number(this.route.snapshot.paramMap.get('id'));
        // Get the page data.
        this.pageService.getPage(
            pageId,
            (pageNode: Node) => {
                this.pageNode = pageNode;
                this.titleService.setTitle('Manage - '+this.pageNode.page.name);
                this.loading = false;
                this.afterPageLoadOrChange();
            }
        );
        // Get the token and its published state.
        this.pageService.getPageToken(
            pageId,
            (response: {isPublished: boolean, token: string}) => {
                this.publishedToken.isPublished = response.isPublished;
                this.publishedToken.token = response.token;
            }
        );

        this.labelService.getLabels();
        this.groupService.loadUserGroups()/*.subscribe()*/; // I'm not sure this is the right way of doing it, but if you don't subscribe, you don't get anything.
        this.groupService.loadSharedNodes(GroupNodes => {
console.log('About to call afterPageLoadOrChange()');
          this.afterPageLoadOrChange();
        });
    }

    ngOnDestroy(): void {
        this.titleService.setTitle('Bookmarks');
    }

  // @TODO: This should be in its own service.
  public onDragStart(targetTypes: string[]) {
    this.draggingTargetTypes = targetTypes;
  }

  // @TODO: This should be in its own service.
  public onDragEnd() {
    this.draggingTargetTypes = [];
  }

    // @TODO: This should be in its own service.
    public showDroppableHint(types: string[]): boolean {
      return this.draggingTargetTypes.filter((draggingType: string) => types.indexOf(draggingType) >= 0).length > 0;
    }

    publish(): void {
        this.apiCallerService.callApi('post', 'publish/page/'+this.pageNode.page.id, true, null, (response: {token: string}) => {
            // Page has been published - redirect to the page.
            this.router.navigate(['published', 'page', response.token]);
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

    unpublishPage(): void {
        this.modalService.open(this.unpublishPageModalId);
    }

    confirmUnpublishPage(): void {
        this.apiCallerService.callApi('delete', 'publish/page/'+this.pageNode.page.id, true, null, (response: HttpResponse<any>) => {
            // Page has been unpublished - clear the token.
            this.publishedToken.isPublished = false;
            this.publishedToken.token = null;

            // Only close the modal when we get a response back.
            this.modalService.close(this.unpublishPageModalId, false);
        }, (error: HttpErrorResponse) => {
          this.errorMessage = error.statusText;
        });
    }

    addPanel(): void {
        this.currentParentNode = null;
        this.addEditPanelModalTitle = 'Add Panel';
        this.panelCopy = new Panel();
        this.errorMessage = '';
        this.modalService.open(this.addEditPanelModalId);
    }

    editPanel(panel: Panel): void {
        this.addEditPanelModalTitle = 'Edit Panel';
        this.originalPanel = panel;
        this.panelCopy = new Panel();
        this.panelCopy.id = panel.id;
        this.panelCopy.title = panel.title;
        this.errorMessage = '';
        this.modalService.open(this.addEditPanelModalId);
    }

    savePanel(): void {
        // Make sure the title is entered.
        if (this.panelCopy.title == '') {
            this.errorMessage = 'Title is mandatory';
            return;
        }

        // Send a POST/PUT request to the server with the new/edited values, wait for the response. If successful,
        // copy the values into the "real" panel object and close the window. Any errors, keep the modal open and
        // show the error.
        let panelData = JSON.stringify({
            title: this.panelCopy.title,
        });
        if (this.panelCopy.id) {
            // They are editing an existing panel.
            this.apiCallerService.callApi('put', 'panels/'+this.panelCopy.id, true, panelData, (response: {panel: {title: string}}) => {
                let panel = response.panel;
                // Copy values back in to the real panel.
                this.originalPanel.title = panel.title;
                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditPanelModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        } else {
            // This is a new panel.
            this.apiCallerService.callApi('post', 'pages/'+this.pageNode.page.id+'/panels', true, panelData, (response: HttpResponse<any>) => {
                // Create a new node/panel with the new data, and sort by sequence number, just in case.
                this.pageNode.childNodes.push(Node.makeNode(response));
                Node.sortBySeq(this.pageNode.childNodes);

                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditPanelModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        }
    }

    addBookmark(parentNode: Node): void {
        // This handles adding a bookmark under either a panel, a section or a cell.
        this.currentParentNode = parentNode;
        this.addEditBookmarkModalTitle = 'Add Bookmark';
        this.bookmarkCopy = new Bookmark();
        this.errorMessage = '';
        this.labelSearch = '';
        this.modalService.open(this.addEditBookmarkModalId);
    }

    editBookmark(bookmark: Bookmark): void {
        this.addEditBookmarkModalTitle = 'Edit Bookmark';
        this.originalBookmark = bookmark;
        this.bookmarkCopy = new Bookmark();
        this.bookmarkCopy.id = bookmark.id;
        this.bookmarkCopy.label = bookmark.label;
        this.bookmarkCopy.url = bookmark.url;
        this.bookmarkCopy.setLabels(bookmark.getActiveLabels());
        this.errorMessage = '';
        this.labelSearch = '';
        this.modalService.open(this.addEditBookmarkModalId);
    }

    deleteObject(deleteNode: Node, parentOfDeletedNode?: Node): void {
        this.deleteObjectModalTitle = 'Delete '+deleteNode.nodeType;
        this.deleteObjectNode = deleteNode;
        this.parentOfDeletedNode = parentOfDeletedNode;
        this.modalService.open(this.deleteObjectModalId);
    }

    handleDeleteObject(): void {
        // They are deleting a node.
        let url = 'nodes/'+this.deleteObjectNode.id;
        if (this.deleteObjectNode.parentNodeId) {
            url = url+'/'+this.deleteObjectNode.parentNodeId;
        }
        this.apiCallerService.callApi('delete', url, true, null, (response: HttpResponse<any>) => {
            // We will receive back the parent node (because the sequence numbers of the siblings of our node
            // may have changed). Replace the child nodes of the parent node of the deleted node with the
            // children of the response.
            // If we deleted a page, there will be nothing, so we actually have to jump back to the pages list.
            if (this.parentOfDeletedNode) {
                this.parentOfDeletedNode.refreshChildren(Node.makeNode(response));
                Node.sortBySeq(this.parentOfDeletedNode.childNodes);
                this.afterPageLoadOrChange(); // Refresh the state of the page and available shared nodes.
                // Only close the modal when we get a response back.
                this.modalService.close(this.deleteObjectModalId, false);
            } else {
                this.router.navigate(['pages'], {queryParams: {pageDeleted: true}});
            }
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

    alreadyOnBookmark(label: Label): boolean {
        for (let labelIndex in this.originalBookmark.labels) {
            if (label == this.originalBookmark.labels[labelIndex]) {
                return true;
            }
        }

        return false;
    }

    saveBookmark(): void {
        // Make sure the label and URL are entered.
        if (this.bookmarkCopy.label == '') {
            this.errorMessage = 'Label is mandatory';
            return;
        }
        // @TODO: Should we check that the URL has a valid format?
        if (this.bookmarkCopy.url == '') {
            this.errorMessage = 'URL is mandatory';
            return;
        }

        // Send a POST/PUT request to the server with the new/edited values, wait for the response. If successful,
        // copy the values into the "real" bookmark object and close the window. Any errors, keep the modal open and
        // show the error.
        let bookmarkData = JSON.stringify({
            label: this.bookmarkCopy.label,
            url: this.bookmarkCopy.url,
            labels: this.bookmarkCopy.getActiveLabels()
        });
        if (this.bookmarkCopy.id) {
            // They are editing an existing bookmark.
            this.apiCallerService.callApi('put', 'bookmarks/'+this.bookmarkCopy.id, true, bookmarkData, (response: {bookmark: {label: string, url: string, labels: string[]}}) => {
                let bookmark = response.bookmark;
                // Copy values back in to the real bookmark.
                this.originalBookmark.label = bookmark.label;
                this.originalBookmark.url = bookmark.url;
                this.originalBookmark.setLabelsFromRaw(bookmark.labels);
                // We need to refresh the "global" set of labels.
                this.labelService.getLabels(this.originalBookmark);
                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditBookmarkModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        } else {
            // This is a new bookmark.
            let endpoint = null;
            switch (this.currentParentNode.nodeType) {
                case Panel.NODE_TYPE:
                    endpoint = 'panels/'+this.currentParentNode.panel.id+'/bookmarks';
                    break;
                case Section.NODE_TYPE:
                    endpoint = 'sections/'+this.currentParentNode.section.id+'/bookmarks';
                    break;
                case Cell.NODE_TYPE:
                    endpoint = 'cells/'+this.currentParentNode.cell.id+'/bookmarks';
                    break;
                default:
                    alert('Unknown parent bookmark type: '+this.currentParentNode.nodeType);
                    return;
            }
            this.apiCallerService.callApi('post', endpoint, true, bookmarkData, (response: HttpResponse<any>) => {
                // Create a new node/bookmark with the new data, and sort by sequence number, just in case.
                let newBookmarkNode = Node.makeNode(response);
                this.currentParentNode.childNodes.push(newBookmarkNode);
                Node.sortBySeq(this.currentParentNode.childNodes);

                // We need to refresh the "global" set of labels.
                this.labelService.getLabels(newBookmarkNode.bookmark);

                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditBookmarkModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        }
    }

    addSection(panelNode: Node): void {
        this.currentParentNode = panelNode;
        this.addEditSectionModalTitle = 'Add Section';
        this.sectionCopy = new Section();
        this.errorMessage = '';
        this.modalService.open(this.addEditSectionModalId);
    }

    editSection(section: Section): void {
        this.addEditSectionModalTitle = 'Edit Section';
        this.originalSection = section;
        this.sectionCopy = new Section();
        this.sectionCopy.id = section.id;
        this.sectionCopy.heading = section.heading;
        this.errorMessage = '';
        this.modalService.open(this.addEditSectionModalId);
    }

    saveSection(): void {
        // There is no validation as the only field is optional.

        // Send a POST/PUT request to the server with the new/edited values, wait for the response. If successful,
        // copy the values into the "real" section object and close the window. Any errors, keep the modal open and
        // show the error.
        let sectionData = JSON.stringify({
            heading: this.sectionCopy.heading,
        });
        if (this.sectionCopy.id) {
            // They are editing an existing section.
            this.apiCallerService.callApi('put', 'sections/'+this.sectionCopy.id, true, sectionData, (response: {section: {heading: string}}) => {
                let section = response.section;
                // Copy values back in to the real section.
                this.originalSection.heading = section.heading;
                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditSectionModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        } else {
            // This is a new section.
            this.apiCallerService.callApi('post', 'panels/'+this.currentParentNode.panel.id+'/sections', true, sectionData, (response: HttpResponse<any>) => {
                // Create a new node/section with the new data, and sort by sequence number, just in case.
                this.currentParentNode.childNodes.push(Node.makeNode(response));
                Node.sortBySeq(this.currentParentNode.childNodes);

                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditSectionModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        }
    }

    addGrid(sectionNode: Node): void {
        this.currentParentNode = sectionNode;
        this.addGridModalTitle = 'Add Grid';
        this.gridCopy = new Grid();
        this.errorMessage = '';
        this.modalService.open(this.addGridModalId);
    }

    saveGrid(): void {
        // @TODO: Are these comments right - there don't seem to be any fields!
        // There is no validation as the only field is optional.

        // Send a POST request to the server, wait for the response. If successful, copy the values into the "real"
        // grid object and close the window. Any errors, keep the modal open and show the error.
        let gridData = JSON.stringify({});

        // At the moment, you can't edit grids (there's nothing to edit!).
        // This is a new grid.
        this.apiCallerService.callApi('post', 'sections/' + this.currentParentNode.section.id + '/grids', true, gridData, (response: HttpResponse<any>) => {
            // Create a new node/grid with the new data, and sort by sequence number, just in case.
            this.currentParentNode.childNodes.push(Node.makeNode(response));
            Node.sortBySeq(this.currentParentNode.childNodes);

            // Only close the modal when we get a response back.
            this.modalService.close(this.addGridModalId, false);
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

    addGridRow(gridNode: Node): void {
        // Don't ask for confirmation, just ask the API to create a new row.
        // @TODO: Hide the "Add row" buttons while we do it, though. Maybe show a spinner?
        this.apiCallerService.callApi('post', 'grids/' + gridNode.grid.id + '/rows', true, {}, (response: HttpResponse<any>) => {
            // Create a new node/row with the new data, and sort by sequence number, just in case.
            // A new row will contain, 1 or more new cells, but makeNode will handle that automatically.
            gridNode.childNodes.push(Node.makeNode(response));
            Node.sortBySeq(gridNode.childNodes);
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

    addGridColumn(gridNode: Node): void {
        // Don't ask for confirmation, just ask the API to create a new column.
        // @TODO: Hide the "Add row" buttons while we do it, though. Maybe show a spinner?
        this.apiCallerService.callApi('post', 'grids/' + gridNode.grid.id + '/columns', true, {}, (response: HttpResponse<any>) => {
            // This is a little different to the other API calls in that it returns the entire grid, so we
            // literally replace our grid object with a new grid object made from the response data.
            // It didn't seem to work, so for now, remove all the rows from the existing grid, and add all
            // the new ones.
            let newGridNode = Node.makeNode(response);
            gridNode.clearChildren();
            for(let rowIndex = 0 ; rowIndex < newGridNode.childNodes.length ; rowIndex++) {
                gridNode.addChild(newGridNode.childNodes[rowIndex]);
            }
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

    isExactMatch(): boolean {
        // @TODO: Do not allow add if the search term exists as an active label in the bookmark.
        for (let labelIndex in this.labelService.labels) {
            if (this.labelService.labels[labelIndex].getName() == this.labelSearch) {
                return true;
            }
        }

        return false;
    }

    matchingLabels(): Label[] {
        let result = [];
        // We need to take the user's search term, and find any matching labels in the global set of labels.
        // but where the label is not already against this bookmark.
        let bookmarkLabels = this.bookmarkCopy.getActiveLabels();
        for (let labelIndex in this.labelService.labels) {
          let lowerSearch = this.labelSearch.toLowerCase();
          if (this.labelService.labels[labelIndex].getName().toLowerCase().indexOf(lowerSearch) !== -1) {
                let found = false;
                for (let bookmarkLabelIndex in bookmarkLabels) {
                    if (this.labelService.labels[labelIndex].getName() == bookmarkLabels[bookmarkLabelIndex].getName()) {
                        found = true;
                    }
                }
                if (!found) {
                    result.push(this.labelService.labels[labelIndex]);
                }
            }
        }

        return result;
    }

    addNewLabel(): void {
        this.bookmarkCopy.addLabel(new Label(null, this.labelSearch));
        this.labelSearch = '';
    }

    addExistingLabel(label: Label): void {
        this.bookmarkCopy.addLabel(label);
    }

    handleNodeDrop(e: any, targetSeq: number, targetParentNode: Node) {
        // Cannot drag panel to certain targets because the sequence number simply wouldn't change!
        let draggedObject: Node = this.pageNode.getDescendentNodeById(e.data.id);
        if (draggedObject.parentNodeId) {
            // It has a parent in this page, so it's a "move".
            this.moveNode(draggedObject, targetParentNode, targetSeq);
        } else {
            // It has no parent in the page, so it's an "embed" of a node shared by someone else.
            this.embedNode(draggedObject, targetParentNode, targetSeq);
        }
    }

    private moveNode(draggedObject: Node, targetParentNode: Node, targetSeq: number) {
        if (draggedObject.parentNodeId == targetParentNode.id) {
            if (draggedObject.sequenceNumber == targetSeq || draggedObject.sequenceNumber == targetSeq + 1) {
                return; // No point doing anything.
            }
        }

        // Call the API to make the change.
        let moveParameters = {
            currentParentNodeId: draggedObject.parentNodeId,
            newParentNodeId: targetParentNode.id,
            newChildIndex: draggedObject.sequenceNumber > targetSeq || draggedObject.parentNodeId != targetParentNode.id ? targetSeq + 1 : targetSeq
        };
        this.apiCallerService.callApi('post', 'nodes/'+draggedObject.id+'/move', true, JSON.stringify(moveParameters), (response: any[]) => {
            for (let resultIdx = 0; resultIdx < response.length; resultIdx++) {
                this.pageNode.refreshChildren(Node.makeNode(response[resultIdx]));
            }
            this.afterPageLoadOrChange();
        }, (error: HttpErrorResponse) => {
            alert('There was a problem moving the node');
        });
    }

    private embedNode(draggedObject: Node, targetParentNode: Node, targetSeq: number) {
        // Call the API to make the change.
        let embedParameters = {
            newParentNodeId: targetParentNode.id,
            newChildIndex: 1+targetSeq
        };
        this.apiCallerService.callApi('post', 'nodes/'+draggedObject.id+'/embed', true, JSON.stringify(embedParameters))
            .subscribe((result): void => {
                    this.pageNode.refreshChildren(Node.makeNode(result));
                    this.afterPageLoadOrChange();
                },
                error => {
                    alert('There was a problem embedding the node');
                });
    }

    public handlePanelDropOnPages(e: any) {
        this.movePanel = true;
        this.panelToMove = e.dragData;
        this.pageService.getPages((pageNodes: Node[]) => {
          this.movePanelBetweenPagesPageList = pageNodes;
          // Exclude the current page from the list.
          for (let pageIdx = this.movePanelBetweenPagesPageList.length - 1 ; pageIdx >= 0 ; pageIdx-- ) {
            if (this.movePanelBetweenPagesPageList[pageIdx].id == this.panelToMove.parentNodeId) {
              this.movePanelBetweenPagesPageList.splice(pageIdx, 1);
            }
          }
          this.selectedPageNodeId = this.movePanelBetweenPagesPageList[0].id;
        })
      // @TODO: Handle errors
      //           error => console.log("Error :: " + error)
      //       );
        this.modalService.open(this.movePanelBetweenPagesModalId);
    }

    public movePage(navigateToDestinationPage: boolean) {
        // Find the node in the page list.
        let pageNode: Node = null;
        for (let pageIdx = 0 ; pageIdx < this.movePanelBetweenPagesPageList.length ; pageIdx++ ) {
            if (this.movePanelBetweenPagesPageList[pageIdx].id == this.selectedPageNodeId) {
                pageNode = this.movePanelBetweenPagesPageList[pageIdx];
            }
        }
        // Determine the new sequence number.
        let newSeq: number = 1;
        if (pageNode.childNodes.length) {
            newSeq = 1 + pageNode.childNodes[pageNode.childNodes.length-1].sequenceNumber;
        }
        // Call the API to make the change.
        let moveParameters = {
            currentParentNodeId: this.pageNode.id,
            newParentNodeId: this.selectedPageNodeId,
            newChildIndex: newSeq
        };
        this.apiCallerService.callApi('post', 'nodes/'+this.panelToMove.id+'/move', true, JSON.stringify(moveParameters))
            .subscribe((result): void => {
                    // If the user wants to navigate to the target page, don't bother refreshing
                    // anything, just navigate to that page.
                    if (navigateToDestinationPage) {
                        this.router.navigate(['page', pageNode.page.id]);
                    } else {
                        // Refresh anything that's changed in this page.
                        for (let resultIdx = 0; resultIdx < result.length; resultIdx++) {
                            this.pageNode.refreshChildren(Node.makeNode(result[resultIdx]));
                        }
                        this.afterPageLoadOrChange();
                    }
                },
                error => {
                    console.log('Error: ', error);
                    alert('There was a problem moving the node');
                });

        this.movePanel = false;
        this.modalService.close(this.movePanelBetweenPagesModalId, false);
    }

    public manageNodeGroups(node: Node): void {
        this.manageGroupNodesModalTitle = 'Share '+node.getDisplayName();
        this.manageGroupsForNode = node;
        this.nodeGroupSettings = [];
        let userGroups = this.groupService.getUserGroups();
        for (let userGroupIndex in userGroups) {
            this.nodeGroupSettings.push(
                new NodeGroupSetting(
                    userGroups[userGroupIndex].groupId,
                    userGroups[userGroupIndex].groupName,
                    node.isSharedWithGroup(userGroups[userGroupIndex].groupId)
                )
            );
        }
        this.managingGroupNodes = true;
        this.modalService.open('manageGroupNodes');
    }

    public saveGroupNodes(): void {
        let callCount = 0;
        // Update (create or delete) any links between the node in focus and the groups.
        for (let nodeGroupSettingIdx in this.nodeGroupSettings) {
            let nodeGroupSetting = this.nodeGroupSettings[nodeGroupSettingIdx];
            let groupId = nodeGroupSetting.getGroupId();
            let nowShared = nodeGroupSetting.isSharedWithGroup();
            let wasShared = this.manageGroupsForNode.isSharedWithGroup(groupId);
            let postOrDelete = null;
            if (nowShared && !wasShared) {
                postOrDelete = 'post';
            } else {
                if (!nowShared && wasShared) {
                    postOrDelete = 'delete';
                }
            }
            if (postOrDelete) {
                callCount++;
                this.apiCallerService.callApi(postOrDelete, 'groups/'+groupId+'/nodes/'+this.manageGroupsForNode.id, true)
                    .subscribe((result): void => {
                            // Node has been added to or removed from the group.
                            // Update the node in memory - add/remove this group from its list.
                            if (postOrDelete == 'post') {
                                // We added it to the group.
                                this.manageGroupsForNode.addToGroup(groupId);
                            } else {
                                // We removed it from the group.
                                this.manageGroupsForNode.removeFromGroup(groupId);
                            }
                            if (--callCount == 0) {
                                // This is the last response - close the modal
                                this.closeManageNodeGroupsPopup();
                            }
                        },
                        error => {
                            // @TODO: Make sure this works in all cases.
                            this.errorMessage = error._body;
                        });
            }
        }
        if (callCount == 0) {
            this.closeManageNodeGroupsPopup();
        }
    }

    private closeManageNodeGroupsPopup() {
        this.managingGroupNodes = false;
        this.modalService.close('manageGroupNodes');
    }

    public getSharedNodesOfType(type: string): GroupNode[] {
        return this.groupService.getSharedNodesOfType(type);
    }

    private afterPageLoadOrChange() {
        this.disableSharedNodesInPage();
        if (this.pageNode) {
            this.pageNode.determineEmbeddedFlags(this.authenticationService.getUserId());
            this.handleFilterChange(); // Filter might be populated when we arrive on the page.
        }
    }

    // When both the page data and the shared nodes data has been loaded, go through all the nodes
    // in the page and if any are in the shared node list, mark them as being in the page.
    private disableSharedNodesInPage() {
        if (this.pageNode && this.groupService.getSharedNodesList()) {
            // Get a list of all the nodes in the page.
            let pageNodeIds = this.pageNode.getAllNodeIds();
            // Go through all the shared nodes and set the "in page" flag depending on whether the node is in the page.
            this.groupService.getSharedNodesList().forEach((sharedNode: GroupNode) => {
                sharedNode.setInPage(pageNodeIds.indexOf(sharedNode.node.id) != -1);
            })
        }
    }

    public clearFilter(): void {
        this.filterService.clear(this.pageNode);
    }

    public handleFilterChange(): void {
        this.filterService.handleFilterChange(this.pageNode);
    }

  protected readonly faTrash = faTrash;
  protected readonly faEyeSlash = faEyeSlash;
  protected readonly faEye = faEye;
  protected readonly faFileText = faFileText;
  protected readonly faSpinner = faSpinner;
  protected readonly faExternalLink = faExternalLink;
  protected readonly faUserGroup = faUserGroup;
  protected readonly faPencilSquare = faPencilSquare;
  protected readonly faPlusSquare = faPlusSquare;
  protected readonly faCaretSquareRight = faCaretSquareRight;
  protected readonly faBars = faBars;
  protected readonly faBookmark = faBookmark;
  protected readonly faCaretSquareDown = faCaretSquareDown;
}
