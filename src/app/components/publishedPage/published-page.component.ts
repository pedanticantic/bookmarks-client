import { Component, OnDestroy, OnInit } from '@angular/core';
import { Node } from "../../models/node";
import {ActivatedRoute, ParamMap, RouterLink} from "@angular/router";
import { PageService } from '../../services/page.service';
import { AppComponent } from "../../app.component";
import { Title } from '@angular/platform-browser';
import { Bookmark } from "../../models/bookmark";
import { LabelService } from "../../services/label.service";
import { Label } from "../../models/label";
import {ModalService} from "../../modal/modal.service";
import {FilterService} from "../../services/filter.service";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faCircle, faCogs, faExclamationTriangle, faTags, faTrash} from '@fortawesome/free-solid-svg-icons';
import {NgClass, NgForOf, NgIf, NgTemplateOutlet} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {ModalComponent} from '../../modal/modal.component';
import {BsDropdownDirective, BsDropdownMenuDirective, BsDropdownToggleDirective} from 'ngx-bootstrap/dropdown';

@Component({
  templateUrl: 'published-page.component.html',
  styleUrls: ['./published-page.component.css'],
  providers: [PageService, LabelService],
  imports: [
    FaIconComponent,
    RouterLink,
    NgClass,
    FormsModule,
    ModalComponent,
    NgIf,
    NgForOf,
    NgTemplateOutlet,
    BsDropdownDirective,
    BsDropdownToggleDirective,
    BsDropdownMenuDirective
  ]
})
export class PublishedPageComponent implements OnDestroy, OnInit {
    pageNode: Node;
    templateBookmark: Bookmark;
    openManually: Bookmark[];

    constructor(
        private titleService: Title,
        private pageService: PageService,
        private route: ActivatedRoute,
        private appComponent: AppComponent,
        private labelService: LabelService,
        private modalService: ModalService,
        public filterService: FilterService
    ) {}

    ngOnInit(): void {
        const token: string = this.route.snapshot.paramMap.get('token');
        this.pageService.getPageByToken(
            token,
            (pageNode: Node) => {
                this.appComponent.showHeading = false;

                this.pageNode = pageNode;
                this.titleService.setTitle(this.pageNode.page.name);
                this.labelService.extractLabels(this.pageNode);
                this.handleFilterChange(); // Filter might be populated when we arrive on the page.
            }
        );
    }

    ngOnDestroy(): void {
        this.appComponent.showHeading = true;
    }

    public labelClick(label: Label, event?: MouseEvent) {
        if (event) {
            event.preventDefault();
        }
        // User has just clicked on that label - need to open all the bookmarks that have that label.
        this.openManually = this.pageNode.openBookmarksByLabel(label);
        if (this.openManually.length) {
            // We must be on Chrome and they tried to open multiple tabs. Chrome will only open a single
            // window on a user operation (eg click), so we must make the user click the 2nd, 3rd, 4th, etc
            // link. It is an absolute pain!
            this.modalService.open('manualOpenPanel');
        }
    }

    public manualOpen(bookmark: Bookmark): void {
        bookmark.parentNode.openBookmark([]);
        for (let bookmarkIdx = this.openManually.length-1 ; bookmarkIdx >= 0 ; bookmarkIdx--) {
            if (this.openManually[bookmarkIdx] == bookmark) {
                this.openManually.splice(bookmarkIdx, 1);
            }
        }
        if (this.openManually.length == 0) {
            this.modalService.close('manualOpenPanel');
        }
    }

    public clearFilter(): void {
        this.filterService.clear(this.pageNode);
    }

    public handleFilterChange(): void {
        this.filterService.handleFilterChange(this.pageNode);
    }

    public getPageLabels(): Label[] {
        return this.labelService.getPageLabels();
    }

  protected readonly faCogs = faCogs;
  protected readonly faTags = faTags;
  protected readonly faCircle = faCircle;
  protected readonly faExclamationTriangle = faExclamationTriangle;
  protected readonly faTrash = faTrash;
}
