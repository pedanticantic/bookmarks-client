import { Component, OnInit } from "@angular/core";
import { ModalService } from "../../modal/modal.service";
import { GroupService } from "../../services/group.service";
import { GroupMember } from "../../models/groupMember";
import { Group } from "../../models/group";
import { ApiCallerService } from "../../services/api-caller.service";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {
  faList,
  faPencilSquare, faPlusSquare,
  faSignOut,
  faSpinner,
  faThumbsDown,
  faThumbsUp, faTrash
} from '@fortawesome/free-solid-svg-icons';

@Component({
  templateUrl: 'groups.component.html',
  providers: [GroupService, ModalService],
  imports: [
    FaIconComponent
  ]
})
export class GroupListComponent implements OnInit {
    loading: boolean;
    groupMemberArray: GroupMember[] = [];
    public addEditGroupModalTitle: string = 'TBA';
    public groupCopy: Group = null;
    public errorMessage: string = null;
    private addEditGroupModalId: string = 'AddEditGroup';
    private originalGroup: GroupMember = null;

    constructor(
        public modalService: ModalService,
        private groupService: GroupService,
        private apiCallerService: ApiCallerService,
    ) {}

    getUserGroups(): void {
        this.loading = true;
        this.groupMemberArray = [];
        this.groupService.loadUserGroups((groups: GroupMember[]) => {
            this.groupMemberArray = groups;
            this.loading = false;
        });
    }

    ngOnInit(): void {
        this.getUserGroups();
    }

    getPendingGroups(): GroupMember[] {
        return this.groupMemberArray.filter((groupMember: GroupMember) => groupMember.status == GroupMember.STATUS_INVITED);
    }

    getAcceptedGroups(): GroupMember[] {
        return this.groupMemberArray.filter((groupMember: GroupMember) => groupMember.status == GroupMember.STATUS_ACCEPTED);
    }

    acceptInvite(groupMember: GroupMember): void {
        this.respondToInvitation(groupMember, 'accept', GroupMember.STATUS_ACCEPTED);
    }

    declineInvite(groupMember: GroupMember): void {
        this.respondToInvitation(groupMember, 'decline', GroupMember.STATUS_DECLINED);
    }

    private respondToInvitation(groupMember: GroupMember, decision: string, newStatus: string): void {
        if (confirm('Are you sure you want to '+decision+' this invitation?')) {
            let update = {status: newStatus};
            this.apiCallerService.callApi('put', 'groups/'+groupMember.groupId+'/members/'+groupMember.userId, true, update)
                .subscribe((result): void => {
                    // It's too difficult to rebuild the data from the result, etc, so just reload it from scratch.
                    this.getUserGroups();
                });
        }
    }

    canEditGroup(groupMember: GroupMember): boolean {
        // We actually receive the group-member record for the group and the logged-in user.
        return groupMember.role == GroupMember.ROLE_ADMIN;
    }

    addGroup(): void {
        this.addEditGroupModalTitle = 'Add Group';
        this.groupCopy = new Group();
        this.errorMessage = '';
        this.modalService.open(this.addEditGroupModalId);
    }

    editGroup(groupMember: GroupMember): void {
        this.addEditGroupModalTitle = 'Edit Group';
        this.originalGroup = groupMember;
        this.groupCopy = new Group();
        this.groupCopy.id = groupMember.groupId;
        this.groupCopy.name = groupMember.groupName;
        this.errorMessage = '';
        this.modalService.open(this.addEditGroupModalId);
    }

    saveGroup(): void {
        // Make sure the name is entered.
        if (this.groupCopy.name == '') {
            this.errorMessage = 'Name is mandatory';
            return;
        }
        // Send a POST/PUT request to the server with the new/edited values, wait for the response. If successful,
        // copy the values into the "real" group object and close the window. Any errors, keep the modal open and
        // show the error.
        let groupData = JSON.stringify({
            name: this.groupCopy.name,
        });
        if (this.groupCopy.id) {
            // They are editing an existing group.
            this.apiCallerService.callApi('put', 'groups/'+this.groupCopy.id, true, groupData)
                .subscribe((result: Group): void => {
                        // Copy values back in to the real group.
                        this.originalGroup.groupName = result.name;
                        // Only close the modal when we get a response back.
                        this.modalService.close(this.addEditGroupModalId, false);
                        GroupMember.sortByGroupName(this.groupMemberArray);
                    },
                    error => {
                        // @TODO: Make sure this works in all cases.
                        this.errorMessage = error._body;
                    });
        } else {
            // This is a new group.
            this.apiCallerService.callApi('post', 'groups', true, groupData)
                .subscribe(result => {
                    // Create a new group-member with the new data, and sort them by group name.
                    let newMember = result.members[0];
                    let groupMemberData = {
                        group: {
                            id: result.id,
                            name: result.name,
                            createdByUserId: result.createdByUserId,
                        },
                        groupMember: {
                            role: newMember.role,
                            status: newMember.status,
                        },
                        user: {
                            id: newMember.userId,
                            givenName: newMember.user.givenName,
                            familyName: newMember.user.familyName
                        }
                    };
                    this.groupMemberArray.push(GroupMember.makeGroupMember(groupMemberData));
                    GroupMember.sortByGroupName(this.groupMemberArray);

                    // Only close the modal when we get a response back.
                    this.modalService.close(this.addEditGroupModalId, false);
                },
                error => {
                    // @TODO: Make sure this works in all cases.
                    this.errorMessage = error._body;
                });
        }
    }

    canLeaveGroup(groupMember: GroupMember): boolean {
        // You can only leave if you're not an admin for the group.
        return groupMember.role !== GroupMember.ROLE_ADMIN;
    }

    leaveGroup(groupMember: GroupMember): void {
        if (this.canLeaveGroup(groupMember)) {
            if (confirm('Are you sure you want to leave this group?')) {
                this.apiCallerService.callApi('delete', 'groups/'+groupMember.groupId+'/members/'+groupMember.userId, true)
                    .subscribe((result): void => {
                        // Was successful - remove the respective group from the list of groups.
                        this.groupMemberArray = this.groupMemberArray.filter(group => group.groupId !== groupMember.groupId);
                    });
            }
        }
    }

    canDeleteGroup(groupMember: GroupMember): boolean {
        // We actually receive the group-member record for the group and the logged-in user.
        return groupMember.role === GroupMember.ROLE_ADMIN;
    }

    deleteGroup(groupMember: GroupMember): void {
        if (this.canDeleteGroup(groupMember)) {
            if (confirm('Are you sure you want to remove this group, its members and objects?')) {
                this.apiCallerService.callApi('delete', 'groups/'+groupMember.groupId, true)
                    .subscribe((result): void => {
                        // Was successful - remove the respective group from the list of groups.
                        this.groupMemberArray = this.groupMemberArray.filter(group => group.groupId !== groupMember.groupId);
                    });
            }
        }
    }

  protected readonly faSpinner = faSpinner;
  protected readonly faThumbsUp = faThumbsUp;
  protected readonly faThumbsDown = faThumbsDown;
  protected readonly faList = faList;
  protected readonly faPencilSquare = faPencilSquare;
  protected readonly faSignOut = faSignOut;
  protected readonly faTrash = faTrash;
  protected readonly faPlusSquare = faPlusSquare;
}
