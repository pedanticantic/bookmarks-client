import { Component, OnInit } from "@angular/core";
import { GroupService } from "../../../services/group.service";
import { ModalService } from "../../../modal/modal.service";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { Group } from "../../../models/group";
import { Title } from "@angular/platform-browser";
import { GroupNode } from "../../../models/GroupNode";
import { NodeHierarchy } from "../../../valueObjects/NodeHierarchy";
import { Node } from "../../../models/node";
import { NodeService } from "../../../services/node.service";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faSpinner} from '@fortawesome/free-solid-svg-icons';

@Component({
  templateUrl: 'groups-nodes.component.html',
  providers: [GroupService, ModalService],
  imports: [
    FaIconComponent
  ]
})
export class GroupNodesListComponent implements OnInit {
    public group: Group;
    public loading: boolean = true;
    public groupNodeArray: GroupNode[];

    constructor(
        private route: ActivatedRoute,
        private groupService: GroupService,
        private titleService: Title,
        private nodeService: NodeService,
    ) {
        this.nodeService.clearNodeHierarchies(); // This is to make sure it refreshes the embedded node data.
    }

    ngOnInit(): void {
        // Get the parent group data.
        this.route.paramMap
            .switchMap((params: ParamMap) => this.groupService.getGroup(+params.get('id')))
            .subscribe(group => {
                this.group = group;
                this.titleService.setTitle('Objects Shared With Group - '+this.group.name);
                // Now we know that the group exists and that the user has access to it, load all the
                // nodes that are currently shared with the group.
                this.groupService.getGroupNodes(this.group.id)
                    .subscribe(nodes => {
                        this.receiveGroupNodes(nodes);
                        this.loading = false;
                    });
            });
    }

    private receiveGroupNodes(nodes: GroupNode[]): void {
        this.groupNodeArray = nodes;
        GroupNode.sortByOwnerAndType(this.groupNodeArray);
    }

    public nodeHierarchyForOwner(node: Node): NodeHierarchy[] {
        return this.nodeService.getHierarchies(node).forOwner();
    }

    public nodeHierarchyForOthers(node: Node): NodeHierarchy[] {
        return this.nodeService.getHierarchies(node).forOthers();
    }

  protected readonly faSpinner = faSpinner;
}
