import {Component, OnInit} from "@angular/core";
import {GroupService} from "../../../services/group.service";
import {ModalService} from "../../../modal/modal.service";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Group} from "../../../models/group";
import {Title} from "@angular/platform-browser";
import {GroupMember} from "../../../models/groupMember";
import {User} from "../../../models/user";
import {ApiCallerService} from "../../../services/api-caller.service";
import {AuthenticationService} from "../../../services/authentication.service";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faEnvelope, faPencil, faSpinner, faTicket, faTrash} from '@fortawesome/free-solid-svg-icons';

@Component({
  templateUrl: 'groups-members.component.html',
  providers: [GroupService, ModalService],
  imports: [
    FaIconComponent
  ]
})
export class GroupMembersListComponent implements OnInit {
    private group: Group;
    public loading: boolean = true;
    public groupMemberArray: GroupMember[] = [];
    private loggedInGroupMember: GroupMember = null; // Link record of this group and the logged in user.

    public blankUser: User = null;
    public errorMessage: string = '';

    public groupMemberOriginal: GroupMember = null;
    public groupMemberCopy: GroupMember = null;

    constructor(
        private titleService: Title,
        private groupService: GroupService,
        private route: ActivatedRoute,
        public modalService: ModalService,
        private apiCallerService: ApiCallerService,
        public authService: AuthenticationService
    ) {}

    ngOnInit(): void {
        // Get the parent group data.
        this.route.paramMap
            .switchMap((params: ParamMap) => this.groupService.getGroup(+params.get('id')))
            .subscribe(group => {
                this.group = group;
                this.titleService.setTitle('Group Members - '+this.group.name);
                // Now we know that the group exists and that the user has access to it, load all the
                // members of the group.
                this.groupService.getGroupMembers(this.group.id)
                    .subscribe(members => {
                        this.receiveGroupMembers(members);
                        this.loading = false;
                    });
            });
    }

    private receiveGroupMembers(members: GroupMember[]): void {
        this.groupMemberArray = members;
        GroupMember.sortByMemberName(this.groupMemberArray);
        this.loggedInGroupMember = this.groupMemberArray.filter(groupMember => groupMember.userId === this.authService.getUserId()).pop();
    }

    showInviteMemberModal(): void {
        this.blankUser = new User();
        this.errorMessage = '';
        this.modalService.open('InviteMember');
    }

    canChangeRole(groupMember: GroupMember): boolean {
        // Logged-in user must be an admin of this group
        return this.loggedInGroupMember.role == GroupMember.ROLE_ADMIN;
    }

    showChangeRole(groupMember: GroupMember): void {
        this.groupMemberOriginal = groupMember;
        this.groupMemberCopy = new GroupMember();
        this.onRoleChange(groupMember.role);
        this.modalService.open('ChangeRole');
    }

    onRoleChange(memberRole: string): void {
        this.groupMemberCopy.role = memberRole;
    }

    changeRole(): void {
        // Only send the request to the server if the role is now different.
        if (this.groupMemberOriginal.role != this.groupMemberCopy.role) {
            let newValues = {
                role: this.groupMemberCopy.role
            };
            this.apiCallerService.callApi('put', 'groups/' + this.group.id + '/members/' + this.groupMemberOriginal.userId, true, newValues)
                .subscribe((groupMember): void => {
                    this.groupMemberOriginal.role = groupMember.role;
                });
        }
        this.modalService.close('ChangeRole', false);
    }

    canRemoveMember(groupMember: GroupMember): boolean {
        // Logged-in user must be an admin of this group; member's current role must not be admin
        return this.loggedInGroupMember.role == GroupMember.ROLE_ADMIN && groupMember.role == GroupMember.ROLE_MEMBER;
    }

    removeMember(groupMember: GroupMember): void {
        if (this.canRemoveMember(groupMember)) {
            if (confirm('Are you sure you want to remove this member from the group?')) {
                this.apiCallerService.callApi('delete', 'groups/'+this.group.id+'/members/'+groupMember.userId, true)
                    .subscribe((members): void => {
                        // Was successful - refresh the list of members.
                        members = members.map(item => {
                            let groupMemberData = {
                                group: item.group,
                                groupMember: item,
                                user: item.user
                            };
                            return GroupMember.makeGroupMember(groupMemberData);
                        });
                        this.receiveGroupMembers(members);
                    });
            }
        }
    }

    private buildGroupsManagementUrl() {
        return window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port : '')+'/groups';
    }

    canResendInvitation(groupMember: GroupMember): boolean {
        // Only if status is pending.
        return groupMember.canResendInvitation();
    }

    resendInvitation(groupMember: GroupMember): void {
        if (confirm('Are you sure you want to resend the invitation?')) {
            // Make a call to the API to invite the member.
            let reInviteData = {
                managementUrl: this.buildGroupsManagementUrl(),
            };
            this.apiCallerService.callApi('post', 'groups/'+groupMember.groupId+'/members/'+groupMember.userId, true, reInviteData)
                .subscribe((result): void => {
                        alert('The invitation was re-sent');
                    },
                    error => {
                        // @TODO: Make sure this works in all cases.
                        this.errorMessage = error._body;
                    });
        }
    }

    inviteMember(): void {
        // @TODO: Make sure the email address is valid.
        if (this.blankUser.email) {
            this.errorMessage = '';
            // Make a call to the API to invite the member.
            let memberData = {
                email: this.blankUser.email,
                managementUrl: this.buildGroupsManagementUrl(),
            };
            this.apiCallerService.callApi('post', 'groups/'+this.group.id+'/members', true, memberData)
                .subscribe((result): void => {
                        let groupMemberData = {
                            group: result.group,
                            groupMember: result,
                            user: result.user
                        };
                        // Add the new group member to the list.
                        this.groupMemberArray.push(GroupMember.makeGroupMember(groupMemberData));
                        GroupMember.sortByMemberName(this.groupMemberArray);
                        this.modalService.close('InviteMember', false);
                    },
                    error => {
                        // @TODO: Make sure this works in all cases.
                        this.errorMessage = error._body;
                    });
        } else {
            this.errorMessage = 'Email must be entered';
        }
    }

  protected readonly faSpinner = faSpinner;
  protected readonly faPencil = faPencil;
  protected readonly faTrash = faTrash;
  protected readonly faEnvelope = faEnvelope;
  protected readonly faTicket = faTicket;
}
