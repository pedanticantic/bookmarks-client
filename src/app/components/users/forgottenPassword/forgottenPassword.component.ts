import {Component} from "@angular/core";
import {ApiCallerService} from "../../../services/api-caller.service";
import {EncryptionService} from "../../../services/encryption.service";
import {faCheckCircle, faSignIn} from '@fortawesome/free-solid-svg-icons';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {NgClass, NgIf} from '@angular/common';
import {RouterLink} from '@angular/router';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';

@Component({
  templateUrl: 'forgotten-password.component.html',
  providers: [EncryptionService],
  imports: [
    FaIconComponent,
    FormsModule,
    NgClass,
    RouterLink,
    NgIf
  ]
})

export class ForgottenPasswordComponent {
    public email: string;
    public loading = false;
    public sent: boolean = false;
    public errorMessage: string;

    constructor(
        private apiCallerService: ApiCallerService,
        private encryptionService: EncryptionService,
    ) {}

    sendResetRequest() {
        this.loading = true;
        let resetData = {
            email: this.email,
            resetPasswordUrl: this.buildPasswordResetUrl(),
            expirySeconds: 60*60*24 // 24 hrs
        };
        this.apiCallerService.callApi('post', 'user/request-password-reset', false, resetData, (response: HttpResponse<any>) => {
            this.sent = true;
            this.loading = false;
        }, (error: HttpErrorResponse) => {
          this.errorMessage = error.error.message;
        });
    }

    private buildPasswordResetUrl() {
        let resetData = this.encryptionService.generatePasswordResetParameters(this.email);

        return window.location.protocol+'//'+window.location.hostname+(window.location.port ? ':'+window.location.port : '')+'/reset-password/'+resetData;
    }

  protected readonly faCheckCircle = faCheckCircle;
  protected readonly faSignIn = faSignIn;
}
