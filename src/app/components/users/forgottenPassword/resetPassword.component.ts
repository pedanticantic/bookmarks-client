import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {EncryptionService} from "../../../services/encryption.service";
import {ApiCallerService} from "../../../services/api-caller.service";
import {faCheckCircle, faQuestionCircle, faSignIn} from '@fortawesome/free-solid-svg-icons';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: 'reset-password.component.html',
  providers: [EncryptionService],
  imports: [
    FaIconComponent
  ]
})

export class ResetPasswordComponent {
    private email?: string;
    private version?: number;
    private check?: string;
    private user?: string;
    public allParametersPresent: boolean = false;
    public parametersAreValid: boolean = false;
    public newPassword?: string;
    public loading: boolean = false;
    public error: string;
    public updated: boolean = false;
    private errorMessage?: string;
    private expiryTime?: number;
    public linkHasExpired: boolean;

    constructor(
        private route: ActivatedRoute,
        private encryptionService: EncryptionService,
        private apiCallerService: ApiCallerService,
    ) {
        // Get the parameters, make sure they're all there, and validate them.
        this.route.queryParams.subscribe(params => {
            this.email = params.hasOwnProperty('email') ? params['email'] : null;
            this.version = params.hasOwnProperty('v') ? +params['v'] : null;
            this.check = params.hasOwnProperty('check') ? params['check'] : null;
            this.expiryTime = params.hasOwnProperty('t') ? +params['t'] : null;
            this.user = params.hasOwnProperty('u') ? params['u'] : null;
            this.allParametersPresent = this.email !== null && this.version !== null && this.check !== null && this.expiryTime != null && this.user != null;
            this.linkHasExpired = ((new Date()).getTime()/1000) > this.expiryTime;
            this.parametersAreValid = this.encryptionService.validatePasswordReset(this.email, this.version, this.check);
        });
    }

    public sendNewPassword()
    {
        let newPasswordData = {
            email: this.email,
            resetPassword: this.newPassword,
            u: this.user,
            expiryTime: this.expiryTime
        };
        this.apiCallerService.callApi('put', 'user/password', false, newPasswordData, (response: {success: boolean}) => {
            this.updated = true;
            this.loading = false;
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

  protected readonly faSignIn = faSignIn;
  protected readonly faQuestionCircle = faQuestionCircle;
  protected readonly faCheckCircle = faCheckCircle;
}
