import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router, RouterLink} from '@angular/router';
import { AuthenticationService } from '../../../services/authentication.service';
import {faCheckCircle, faQuestionCircle} from '@fortawesome/free-solid-svg-icons';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {FormsModule} from '@angular/forms';
import {NgClass, NgIf} from '@angular/common';

@Component({
  // moduleId: module.id,
  imports: [
    FaIconComponent,
    FormsModule,
    NgClass,
    RouterLink,
    NgIf
  ],
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    error = '';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    ngOnInit() {
        // reset login status
        this.authenticationService.logout(false);

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/pages';
    }

    login() {
        this.loading = true;
        this.authenticationService.login(
            this.model.username,
            this.model.password,
            (result: boolean) => {
                if (result === true) {
                    // login successful so redirect to return url
                    this.router.navigateByUrl(this.returnUrl);
                } else {
                    // login failed
                    this.error = 'Username or password is incorrect';
                    this.loading = false;
                }
            }
        );
    }

  protected readonly faQuestionCircle = faQuestionCircle;
  protected readonly faCheckCircle = faCheckCircle;
}
