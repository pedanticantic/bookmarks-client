import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {RegisterService} from '../../../services/register.service';
import {AuthenticationService} from '../../../services/authentication.service';
// import {RecaptchaLoaderService} from 'ng-recaptcha';
import {ApiCallerService} from '../../../services/api-caller.service';
import {Observable, throwError} from 'rxjs';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {NgClass, NgIf} from '@angular/common';

@Component({
  // moduleId: module.id,
  templateUrl: 'register.component.html',
  imports: [
    FormsModule,
    NgClass,
    NgIf
  ],
  providers: [
    // RecaptchaLoaderService // This might only be needed because we're now on an earlier version?
  ]
})

export class RegisterComponent implements OnInit {
    model: any = {};
    loading = false;
    error = '';
    recaptchaSuccess = true; //false; // @TODO: Switch this back when I re-enable re-captcha

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private registerService: RegisterService,
        private apiCallerService: ApiCallerService
    ) {}

    ngOnInit() {
        // reset register status
        this.authenticationService.logout(false);
    }

    register() {
        this.loading = true;
        this.registerService.register(this.model.name, this.model.familyName, this.model.email, this.model.password, (result: boolean) => {
            if (result === true) {
                // Registration successful
                this.authenticationService.login(
                    this.model.email,
                    this.model.password,
                    (result: boolean) => {
                        if (result === true) {
                            this.router.navigate(['/pages']);
                        } else {
                            // login failed
                            this.error = 'Something went wrong after registration';
                        }
                    }
                );
            } else {
                // Registration failed
                this.error = 'Registration failed';
                this.loading = false;
            }
        });
    }

    // @TODO: I don't think this is used ATM.
    resolved(captchaResponse: string) {
        // Assume failure until we know for sure that it was successful.
        this.recaptchaSuccess = false;

        if (captchaResponse === null) {
            // The "success" has timed out, the button is disabled.
            return;
        }

        // Make a request to our API, which will talk to Google, who will verify the token.
        const mapCallback = (response: {success: boolean}) => {
alert('What does the response in console log look like? [mapCallback]');
console.log('response:', response);
          if (response.hasOwnProperty('success') && response.success) {
              // Yes! Success.
              this.recaptchaSuccess = true;
          }
        };
        this.apiCallerService.callApi('get', 'recaptcha-site-verify?captchaResponse=' + captchaResponse, false, null, mapCallback, (error: HttpErrorResponse) => {
            throwError(() => new Error(error.statusText));
        });
    }
}
