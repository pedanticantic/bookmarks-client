import { Component, OnInit } from "@angular/core";
import { AuthenticationService } from "../../../services/authentication.service";
import { ModalService } from "../../../modal/modal.service";
import { ApiCallerService } from "../../../services/api-caller.service";
import {PageService} from "../../../services/page.service";
import {GroupService} from "../../../services/group.service";
import {GroupMember} from "../../../models/groupMember";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faCheck, faPencilSquare, faTimes, faTrash} from '@fortawesome/free-solid-svg-icons';
import {ModalComponent} from '../../../modal/modal.component';
import {FormsModule} from '@angular/forms';
import {NgIf} from '@angular/common';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: 'profile.component.html',
  imports: [
    FaIconComponent,
    ModalComponent,
    FormsModule,
    NgIf
  ]
})
export class ProfileComponent implements OnInit {
    public editMode: string;
    public editProfileModalTitle: string;
    public userCopy: {
        email: string;
        currentPassword: string;
        newPassword: string;
        confirmPassword: string;
        givenName: string;
        familyName: string
    };
    public errorMessage: string;
    public hasPages?: boolean = null;
    public hasGroups?: boolean = null;

    constructor(
        public modalService: ModalService,
        private apiCallerService: ApiCallerService,
        public authService: AuthenticationService,
        private pageService: PageService,
        private groupService: GroupService,
    ) {}

    ngOnInit(): void {
        this.authService.getLoggedInUserDetails();
        this.pageService.getPages((pageNodes: Node[]) => {
          this.hasPages = pageNodes.length > 0;
        });
        this.groupService.loadUserGroups((groups: GroupMember[]) => {
            groups = groups.filter(
                groupMember => groupMember.status != GroupMember.STATUS_INVITED
            );
            this.hasGroups = groups.length > 0;
        });
    }

    requestChangeEmail() {
        this.editMode = 'email';
        this.editProfileModalTitle = 'Change your email address';
        this.showChangeModal();
    }

    requestChangePassword() {
        this.editMode = 'password';
        this.editProfileModalTitle = 'Change your password';
        this.showChangeModal();
    }

    requestChangeNames() {
        this.editMode = 'names';
        this.editProfileModalTitle = 'Change your name(s)';
        this.showChangeModal();
    }

    showChangeModal() {
        this.errorMessage = '';
        this.userCopy = {
            email: this.authService.getEmail(),
            currentPassword: null,
            newPassword: null,
            confirmPassword: null,
            givenName: this.authService.getGivenName(),
            familyName: this.authService.getFamilyName()
        };
        this.modalService.open('editProfile');
    }

    saveChanges() {
        this.errorMessage = null;
        let newDetails = {email: null, currentPassword: null, newPassword: null, name: null, familyName: null};
        if (this.editMode == 'names') {
            if (!this.userCopy.familyName) {
                this.errorMessage = 'Family name must be entered';
            }
            if (!this.userCopy.givenName) {
                this.errorMessage = 'Given name must be entered';
            }
            newDetails.name = this.userCopy.givenName;
            newDetails.familyName = this.userCopy.familyName;
        }
        if (this.editMode == 'password') {
            if (this.userCopy.confirmPassword != this.userCopy.newPassword) {
                this.errorMessage = 'Confirmed password must be the same as the new password';
            }
            if (!this.userCopy.confirmPassword) {
                this.errorMessage = 'Confirmed password must be entered';
            }
            if (!this.userCopy.newPassword) {
                this.errorMessage = 'New password must be entered';
            }
            newDetails.newPassword = this.userCopy.newPassword;
        }
        if (this.editMode == 'email' || this.editMode == 'password') {
            if (!this.userCopy.currentPassword) {
                this.errorMessage = 'Current password must be entered';
            }
            newDetails.currentPassword = this.userCopy.currentPassword;
        }
        if (this.editMode == 'email') {
            newDetails.email = this.userCopy.email;
        }

        if (this.errorMessage == null) {
            // Entries seem correct. Send them to the server and handle the response.
            this.apiCallerService.callApi('put', 'user', true, newDetails, (response: {email: string, name: string, familyName: string}) => {
                this.authService.updateUser(response);

                // Only close the modal when we get a response back.
                this.modalService.close('editProfile', false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        }
    }

    public requestDeleteAccount() {
        this.modalService.open('deleteAccount');
    }

    public deleteAccount() {
        // Send the request to delete the account. On success, jump to the home page.
        this.apiCallerService.callApi('delete', 'user', true, null, () => {
            this.authService.logout(true);
        }, (error: HttpErrorResponse) => {
            this.errorMessage = error.statusText;
        });
    }

  protected readonly faPencilSquare = faPencilSquare;
  protected readonly faTimes = faTimes;
  protected readonly faCheck = faCheck;
  protected readonly faTrash = faTrash;
}
