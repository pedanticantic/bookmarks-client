import {Component} from "@angular/core";
import {AuthenticationService} from "../../../services/authentication.service";
import {ApiCallerService} from "../../../services/api-caller.service";
import {ActivatedRoute} from "@angular/router";
import {FormsModule} from '@angular/forms';
import {NgClass, NgForOf, NgIf} from '@angular/common';
import {HttpResponse} from '@angular/common/http';

@Component({
  templateUrl: 'contact-us.component.html',
  imports: [
    FormsModule,
    NgClass,
    NgIf,
    NgForOf
  ]
})

export class ContactUsComponent {
    public feedbackSent: boolean = false;
    public emailSource: string;
    public email: string;
    public subjectOptions: any[];
    public subject: string = '';
    public details: string;

    constructor(
        public authService: AuthenticationService,
        private apiCallerService: ApiCallerService,
        private route: ActivatedRoute
    ) {
        this.subjectOptions = [];
        this.subjectOptions.push({code: '', label: 'Please select...'});
        this.subjectOptions.push({code: 'didnotregister', label: 'I did not register'});
        this.subjectOptions.push({code: 'didnotrequestpasswordreset', label: 'I did not request a password reset'});
        this.subjectOptions.push({code: 'didnotchangeemail', label: 'I did not change my email address'});
        this.subjectOptions.push({code: 'didnotchangepassword', label: 'I did not change my password'});
        this.subjectOptions.push({code: 'didnotchangeprofile', label: 'I did not change my profile'});
        this.subjectOptions.push({code: 'other', label: 'Other'});

        this.emailSource = this.authService.authenticated() ? 'account' : 'enter';

        this.route.queryParams.subscribe(params => {
            // If the email is present...
            if (params.hasOwnProperty('email')) {
                let emailParam = params['email'];
                // If the user is logged in, and the email matches their account, do nothing because it will default
                // to "account" anyway. Otherwise switch to "use this email", and put the address in the field.
                if (!this.authService.authenticated() || this.authService.getEmail() != emailParam) {
                    this.emailSource = 'enter';
                    this.email = emailParam;
                }
            }
            // If the subject is present, see if it's a valid one. If not, use "other".
            if (params.hasOwnProperty('subject')) {
                let subjectParam = params['subject'];
                let subjectMatch = this.subjectOptions.filter((option) => option.code == subjectParam);
                this.subject = subjectMatch.length == 1 ? subjectParam : 'other';
            }
        });
    }

    public sendFeedback() {
        // Send the details to the API.
        let contactData = {
            email: this.authService.authenticated() && this.emailSource == 'account' ? this.authService.getEmail() : this.email,
            subject: this.subject,
            details: this.details
        };
        this.apiCallerService.callApi('post', 'contact/', false, contactData, (response: HttpResponse<any>) => {
            this.feedbackSent = true;
        });
    }
}
