import {Node} from "../../models/node";
import {AbstractElement} from "../../models/abstractElement";

export class Page extends AbstractElement {
    public static readonly NODE_TYPE = 'page';

    id: number;
    nodeId: number;
    name: string = '';
    description: string = '';
    token: string;

    public static makePage(entity, parentNode: Node) {
        let page = new Page();

        page.id = entity.id;
        page.nodeId = entity.nodeId;
        page.name = entity.name;
        page.description = entity.description;
        page.token = entity.token;

        page.parentNode = parentNode;

        return page;
    }

    filterMatches(filter: string): boolean {
        return false; // We have to say a page doesn't match, to force everything else to do the check.
    }

    public isPublished(): boolean {
        return this.token != null;
    }

    public getToken(): string {
        return this.token;
    }
}