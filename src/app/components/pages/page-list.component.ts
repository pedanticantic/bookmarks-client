import { Component, OnInit } from '@angular/core';
import { Node } from "../../models/node";
import { Page } from "./page";
import { PageService } from '../../services/page.service';
import { ModalService } from '../../modal/modal.service';
import { ApiCallerService } from "../../services/api-caller.service";
import {ActivatedRoute, RouterLink} from "@angular/router";
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {faCheck, faCogs, faEye, faPencilSquare, faPlusSquare, faSpinner} from '@fortawesome/free-solid-svg-icons';
import {ModalComponent} from '../../modal/modal.component';
import {FormsModule} from '@angular/forms';
import {NgForOf, NgIf} from '@angular/common';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  templateUrl: 'pages.component.html',
  providers: [PageService, ModalService],
  imports: [
    FaIconComponent,
    RouterLink,
    ModalComponent,
    FormsModule,
    NgIf,
    NgForOf
  ]
})
export class PageListComponent implements OnInit {
    addEditPageModalId: string = 'AddEditPage';
    addEditPageModalTitle: string = 'TBA';
    pageNodesArray: Node[] = [];
    originalPage: Page = null;
    pageCopy: Page = null;
    errorMessage: string = '';
    loading: boolean;
    pageDeleted: boolean;

    constructor(
        public modalService: ModalService,
        private pageService: PageService,
        private apiCallerService: ApiCallerService,
        private route: ActivatedRoute
    ) {
console.log('Page constructor...');
        this.route.queryParams.subscribe(params => {
            this.pageDeleted = params.hasOwnProperty('pageDeleted') && params['pageDeleted'] == 'true';
        });
    }

    getPages(): void {
        this.pageService.getPages((resultArray: Node[]) => {
          this.pageNodesArray = resultArray;
          this.loading = false;
        });
    }

    ngOnInit(): void {
console.log('ngOnInit()...');
        this.loading = true;
        this.getPages();
    }

    addPage(): void {
console.log('addPage()...');
        this.addEditPageModalTitle = 'Add Page';
        this.pageCopy = new Page();
        this.errorMessage = '';
        this.modalService.open(this.addEditPageModalId);
    }

    editPage(whichPage: Page): void {
        this.addEditPageModalTitle = 'Edit Page';
        this.originalPage = whichPage;
        this.pageCopy = new Page();
        this.pageCopy.id = whichPage.id;
        this.pageCopy.name = whichPage.name;
        this.pageCopy.description = whichPage.description;
        this.errorMessage = '';
        this.modalService.open(this.addEditPageModalId);
    }

    savePage(): void {
        // Make sure the name is entered.
        if (this.pageCopy.name == '') {
            this.errorMessage = 'Name is mandatory';
            return;
        }
        // Send a POST/PUT request to the server with the new/edited values, wait for the response. If successful,
        // copy the values into the "real" page object and close the window. Any errors, keep the modal open and
        // show the error.
        let pageData = JSON.stringify({
            name: this.pageCopy.name,
            description: this.pageCopy.description,
        });
        if (this.pageCopy.id) {
            // They are editing an existing page.
            this.apiCallerService.callApi('put', 'pages/'+this.pageCopy.id, true, pageData, (result: Node): void => {
                let page = result.page;
                // Copy values back in to the real page.
                this.originalPage.name = page.name;
                this.originalPage.description = page.description;
                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditPageModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        } else {
            // This is a new page.
            this.apiCallerService.callApi('post', 'pages', true, pageData, (result) => {
                // Create a new node/page with the new data, and sort by sequence number, just in case.
                this.pageNodesArray.push(Node.makeNode(result));
                Node.sortBySeq(this.pageNodesArray);

                // Only close the modal when we get a response back.
                this.modalService.close(this.addEditPageModalId, false);
            }, (error: HttpErrorResponse) => {
                this.errorMessage = error.statusText;
            });
        }
    }

  protected readonly faSpinner = faSpinner;
  protected readonly faCheck = faCheck;
  protected readonly faEye = faEye;
  protected readonly faCogs = faCogs;
  protected readonly faPencilSquare = faPencilSquare;
  protected readonly faPlusSquare = faPlusSquare;
}
