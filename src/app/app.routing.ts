// Here we're also including the CanActivate API
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/users/login/login.component';
import { AuthGuard } from './guards/auth-guard';
import { HomeComponent } from "./components/home/home.component";
import { PageListComponent } from './components/pages/page-list.component';
import { ManagePageComponent } from "./components/page/manage-page.component";
import { RegisterComponent } from "./components/users/register/register.component";
import {DndComponent} from './components/page/dnd.component';
import { PublishedPageComponent } from "./components/publishedPage/published-page.component";
// import { GroupListComponent } from "./components/groups/group-list.component";
// import { GroupMembersListComponent } from "./components/groups/members/group-members-list-component";
// import { GroupNodesListComponent } from "./components/groups/nodes/group-nodes-list-component";
import { ProfileComponent } from "./components/users/profile/profile.component";
import { ForgottenPasswordComponent } from "./components/users/forgottenPassword/forgottenPassword.component";
// import {ResetPasswordComponent} from "./components/users/forgottenPassword/resetPassword.component";
import {ContactUsComponent} from "./components/users/contactUs/contact-us.component";

export const appRoutes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full'},
    { path: 'home', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'register', component: RegisterComponent},
    { path: 'forgotten-password', component: ForgottenPasswordComponent},
    // { path: 'reset-password', component: ResetPasswordComponent},
    { path: 'contact-us', component: ContactUsComponent},
    { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard]},
    { path: 'pages', component: PageListComponent, canActivate: [AuthGuard]},
    { path: 'page/:id', component: ManagePageComponent, canActivate: [AuthGuard]},
    { path: 'dnd/:id', component: DndComponent, canActivate: [AuthGuard]},
    // { path: 'groups', component: GroupListComponent, canActivate: [AuthGuard]},
    // { path: 'groups/:id/members', component: GroupMembersListComponent, canActivate: [AuthGuard]},
    // { path: 'groups/:id/nodes', component: GroupNodesListComponent, canActivate: [AuthGuard]},
    { path: 'published/page/:token', component: PublishedPageComponent},
    // { path: '**', redirectTo: 'home'}
];

export const routing = RouterModule.forRoot(appRoutes);
