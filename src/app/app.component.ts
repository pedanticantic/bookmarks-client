import {Component} from '@angular/core';
// First and foremost we'll include our authentication service
import { AuthenticationService } from './services/authentication.service';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ActivatedRoute, RouterLink, RouterLinkActive, RouterModule, RouterOutlet} from '@angular/router';
import {faComment, faHome, faSignIn, faSignOut, faThList, faUser, faUserPlus, faUsers} from '@fortawesome/free-solid-svg-icons';
import {NgIf} from '@angular/common';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css'],
  imports: [FontAwesomeModule, NgIf, RouterLink, RouterLinkActive, RouterOutlet],
  providers: [AuthenticationService/*, ActivatedRoute*/]
})
export class AppComponent {
    showHeading: boolean = true;

    // We'll need to include a reference to our authService in the constructor to gain access to the
    // API's in the view.
    constructor(public authService: AuthenticationService) {}

  protected readonly faHome = faHome;
  protected readonly faThList = faThList;
  protected readonly faUsers = faUsers;
  protected readonly faSignIn = faSignIn;
  protected readonly faUserPlus = faUserPlus;
  protected readonly faUser = faUser;
  protected readonly faSignOut = faSignOut;
  protected readonly faComment = faComment;
}
