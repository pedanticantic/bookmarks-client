import { Component, Input, OnInit, HostListener } from '@angular/core';
import { ModalService } from './modal.service';
import {FaIconComponent} from '@fortawesome/angular-fontawesome';
import {NgClass, NgIf} from '@angular/common';
import {faWindowClose} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-modal',
  styleUrls: ['./modal.css'],
  imports: [
    FaIconComponent,
    NgClass,
    NgIf
  ],
  // @TODO: Delete this line when I get SCSS working again.
  // styleUrls: ['./modal.scss'], // @TODO: Reinstate this line when I get SCCS working again.
  templateUrl: 'modal.html'
})
export class ModalComponent implements OnInit {
    @Input() modalId: string;
    @Input() modalTitle: string;
    @Input() blocking = false;
    isOpen = false;

    @HostListener('keyup') onMouseEnter(event) {
        // @TODO: Sort this out!
        // this.keyup(event);
    }

    constructor(private modalService: ModalService) {
    }

    ngOnInit() {
        this.modalService.registerModal(this);
    }

    close(checkBlocking = false): void {
        this.modalService.close(this.modalId, checkBlocking);
    }

    private keyup(event: KeyboardEvent): void {
        if (event.keyCode === 27) {
            this.modalService.close(this.modalId, true);
        }
    }

  protected readonly faWindowClose = faWindowClose;
}
