import { NgModule } from '@angular/core';
import {bootstrapApplication, BrowserModule, Title} from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { AuthGuard } from './guards/auth-guard';
import { AuthenticationService } from './services/authentication.service';
import { PageService } from './services/page.service';
import { HomeComponent } from "./components/home/home.component";
import { LoginComponent } from './components/users/login/login.component';
import { PageListComponent }   from './components/pages/page-list.component';
import { ManagePageComponent } from "./components/page/manage-page.component";
// import { HttpModule } from "@angular/http";
import { ModalService } from './modal/modal.service';
import { ModalComponent } from './modal/modal.component';
import { ApiCallerService } from './services/api-caller.service';
import { ApiConfigService } from "./services/api-config.service";
import { RegisterComponent } from "./components/users/register/register.component";
import { RegisterService } from "./services/register.service";
import { PublishedPageComponent } from "./components/publishedPage/published-page.component";
// import { NgbModule } from 'ngx-bootstrap';
import { LabelService } from "./services/label.service";
import { DndModule } from 'ngx-drag-drop';
import { GroupListComponent } from "./components/groups/group-list.component";
import { GroupService } from "./services/group.service";
import { GroupMembersListComponent } from "./components/groups/members/group-members-list-component";
import { GroupNodesListComponent } from "./components/groups/nodes/group-nodes-list-component";
import { NodeService } from "./services/node.service";
import { ProfileComponent } from "./components/users/profile/profile.component";
import {ForgottenPasswordComponent} from "./components/users/forgottenPassword/forgottenPassword.component";
import {ResetPasswordComponent} from "./components/users/forgottenPassword/resetPassword.component";
import {ContactUsComponent} from "./components/users/contactUs/contact-us.component";
import {FilterService} from "./services/filter.service";
// import { RecaptchaModule } from 'ng-recaptcha';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {ActivatedRoute, RouterModule} from '@angular/router';

@NgModule({
  imports: [
    // BrowserModule,
    FormsModule,
    // routing,
    // NgbModule.forRoot(),
    // FontAwesomeModule,
    DndModule,
    // AppComponent,
    // HomeComponent,
    // LoginComponent,
    // PageListComponent,
    // ModalComponent,
    // ManagePageComponent,
    // RegisterComponent,
    // PublishedPageComponent,
    // GroupListComponent,
    // GroupMembersListComponent,
    // GroupNodesListComponent,
    // ProfileComponent,
    // ForgottenPasswordComponent,
    // ResetPasswordComponent,
    // ContactUsComponent,
    // RecaptchaModule,
    // RouterModule.forRoot([]),
  ],
    declarations: [
    ],
    providers: [
        AuthGuard,
        // AuthenticationService,
        // Title,
        // PageService,
        // ModalService,
        // ApiCallerService,
        // ApiConfigService,
        RegisterService,
        // LabelService,
        // GroupService,
        // NodeService,
        // FilterService,
        // ActivatedRoute
    ],
    bootstrap: [/*AppComponent*/],
})
export class AppModule { }
